//
//  InlineTextField.swift
//  Share My Journey
//
//  Created by Robert John Britton on 06/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit

/// A view that contains a label and a textField which are displayed inline from left to right respectively.
class InlineTextFieldView: UIView {
    
    public var label = UILabel()
    
    public var textField = UITextField()
    
    override func didMoveToSuperview() {
        addSubview(label)
        addSubview(textField)
        
        setupView()
    }
    
    private func setupView() {
        label.snp.makeConstraints({(make) -> Void in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview()
            make.width.equalToSuperview().dividedBy(3)
            make.height.equalToSuperview()
        })
        
        textField.snp.makeConstraints({(make) -> Void in
            make.centerY.equalToSuperview()
            make.left.equalTo(label.snp.right)
            make.right.equalToSuperview()
            make.height.equalToSuperview()
        })
    }
}
