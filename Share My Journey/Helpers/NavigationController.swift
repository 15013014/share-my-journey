//
//  NavigationController.swift
//  Share My Journey
//
//  Created by Robert John Britton on 15/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit

// A Navigation controller with a blue navigation bar and white text and border shadows removed
class NavigationController: UINavigationController {

    override init(rootViewController: UIViewController) {
        super.init(rootViewController: rootViewController)
        
        // Set the tint and background colour of the navigation bar view
        self.navigationBar.backgroundColor = UIColor(hex: Global.colour.darkBlue)
        self.navigationBar.barTintColor = UIColor(hex: Global.colour.darkBlue)
        
        
        // Remove the default shadow underneath navigation bar
        self.navigationBar.shadowImage = UIImage()
        
        // Sets the colour for all text that appears in the navigation bar (ie. button / title text)
        self.navigationBar.tintColor = UIColor.white
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        self.navigationBar.titleTextAttributes = textAttributes
        
        // Doesn't display blurred content behind the navigation bar (if a scrollview content moves behind the navigation bar)
        self.navigationBar.isTranslucent = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .default
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }

}
