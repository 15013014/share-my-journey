//
//  NavigationControllerLight.swift
//  Share My Journey
//
//  Created by Robert John Britton on 15/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit

// A Navivigation controller that has a white navigation bar with blue text colour with no border shadows removed.
class NavigationControllerLight: UINavigationController {

    // Setup the navigation controllers initial styling during initialisation procedure
    override init(rootViewController: UIViewController) {
        super.init(rootViewController: rootViewController)
        
        // Set the tint and background colour of the navigation bar view
        self.navigationBar.backgroundColor = UIColor.white
        self.navigationBar.barTintColor = UIColor.white
        
        // Removes the bottom border shadow from the navigation bar
        self.navigationBar.shadowImage = UIImage()
        
        // Sets the colour for all text that appears in the navigation bar (ie. button / title text)
        self.navigationBar.tintColor = UIColor(hex: Global.colour.darkBlue)
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor(hex: Global.colour.darkBlue)]
        self.navigationBar.titleTextAttributes = textAttributes
        
        // Doesn't display blurred content behind the navigation bar (if a scrollview content moves behind the navigation bar)
        self.navigationBar.isTranslucent = false
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }

}
