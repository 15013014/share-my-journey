//
//  TextField.swift
//  Share My Journey
//
//  Created by Robert John Britton on 08/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit

/// A rectangular customised textfield that allows all general textfield behaviours with added control over limiting characters
public class BaseTextField: UITextField {
    
    /// When set, adds a lstener to the textfield that filters input characters that fall outside of the range specified
    /// Remove the listener by specifying nil
    public var validCharacters: CharacterSet? {
        didSet {
            if validCharacters != nil {
                self.addTarget(self, action: #selector(editingChanged) , for: .editingChanged)
                return
            }
            self.removeTarget(self, action: #selector(editingChanged) , for: .editingChanged)
        }
    }
    
    /// When set adds a lstener that removes inputs characters after the specified limit has been reached
    /// Remove the listener by specifying nil
    public var characterLimit: Int? {
        didSet {
            if characterLimit != nil {
                self.addTarget(self, action: #selector(checkValidLength) , for: .editingChanged)
                return
            }
            self.removeTarget(self, action: #selector(checkValidLength) , for: .editingChanged)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
                
        setLeftContentInset(8)
        backgroundColor = UIColor(hex: Global.colour.white)
        self.clearButtonMode = .whileEditing
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /// Provides padding of the specified amount to the inner-left side of the textfield
    /// Remove padding by specfying nil
    func setLeftContentInset(_ width: CGFloat?) {
        if let left = width {
            let leftInsetView = UIView(frame: CGRect(x: 0, y: 0, width: left , height: self.frame.height))
            self.leftView = leftInsetView
            self.leftViewMode = .always
            return
        }
        
        self.leftView = nil
        self.leftViewMode = .never
    }
        
    @objc private func editingChanged() {
        // Check the textfield is not empty
        guard let text: String = self.text, !text.isEmpty else { return }
        
        // Apply Filter the current string
        let filtered = text.components(separatedBy: validCharacters!.inverted).joined(separator: "")
        
        // Set text as filtered text
        self.text = filtered
    }
    
    @objc private func checkValidLength() {
        // Count the number of chars in text, remove last when limit reached
        if let count = self.text?.count {
            if count > characterLimit! {
                self.text?.removeLast()
            }
        }
    }
    
}


/// A rounded customised textfield that allows all general textfield behaviours with added control over limiting characters
public class RoundedTextField: BaseTextField {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.layer.cornerRadius = 8
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor(hex: Global.colour.lightGrey).cgColor
        self.clipsToBounds = true
        
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}




/// A rectangular textfield that prohibits the user from directly entering text. Useful for displaying information that should not be edited.
public class ReadOnlyTextField: BaseTextField {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.isUserInteractionEnabled = false
        self.clearButtonMode = .never
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

/// A rounded textfield that prohibits the user from directly entering text. Useful for displaying information that should not be edited.
public class RoundedReadOnlyTextField: RoundedTextField {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.isUserInteractionEnabled = false
        self.clearButtonMode = .never
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}




/// A rectangular textfield that prohibits the user pasting text. Suitable for use when only pre-defined entries are allowed, such as, contents from a picker or dropdown selection menu
public class SelectionTextField: BaseTextField {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.clearButtonMode = .never
    }
    
    required public init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    // Disbale the users abaility to paste text into a selection textField, as this usually involves checking a number
    override public func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        // Make the clipboard menu invisible
        UIMenuController.shared.isMenuVisible = false
        
        if action == #selector(UIResponderStandardEditActions.paste(_:)) {
            return false
        } else {
            return super.canPerformAction(action, withSender: sender)
        }
        
    }
}

/// A rounded textfield that prohibits the user pasting text. Suitable for use when only pre-defined entries are allowed, such as, contents from a picker or dropdown selection menu
public class RoundedSelectionTextField: RoundedTextField {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.clearButtonMode = .never
    }
    
    required public init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    // Disbale the users abaility to paste text into a selection textField, as this usually involves checking a number
    override public func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        // Make the clipboard menu invisible
        UIMenuController.shared.isMenuVisible = false
        
        if action == #selector(UIResponderStandardEditActions.paste(_:)) {
            return false
        } else {
            return super.canPerformAction(action, withSender: sender)
        }
    }
}






/// A rectangular textfield that prompts the user to change the text by displaying a clear text icon at all times
public class PromptTextField: BaseTextField {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.clearButtonMode = .always
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

/// A rounded textfield that prompts the user to change the text by displaying a clear text icon at all times
public class RoundedPromptTextField: RoundedTextField {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.clearButtonMode = .always
    }
    
    required public  init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}













