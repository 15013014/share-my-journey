//
//  DateFormatter.swift
//  Share My Journey
//
//  Created by Robert John Britton on 28/03/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit

/// Contains static methods that handle the conversion of date related objects.
class FormatDate {
    
    /// Formats a given date object into an integer timeInterval which is suitable for storing in a database
    static func formateDateToIntegerTimeInterval(date: Date) -> Int {
        let dateToTimeInterval = date.timeIntervalSince1970
        let dateTimeIntervalToInt = Int(dateToTimeInterval)
        return dateTimeIntervalToInt
    }
    
    static func formatInegerTimeInetervalToDate(timeInterval: Int) -> Date {
        return Date(timeIntervalSince1970: TimeInterval(timeInterval))
    }
    
    
    /// Returns a Date object from an integer time interval. Uses reference to timeIntervalSince1970 to achieve the date.
    static func formatIntegerTimeIntervalToDateString(timeInterval: Int) -> String {
        let date: Date = Date(timeIntervalSince1970: TimeInterval(timeInterval))
        let dateAsString = formatDateToString(date: date)
        return dateAsString
    }
    
    /// Formats a Date object into a human readable String which in the form "dd/MM/yyyy"
    static func formatDateToString(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let dateString = dateFormatter.string(from: date)
        return dateString
    }

}
