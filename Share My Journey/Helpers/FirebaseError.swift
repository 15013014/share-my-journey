//
//  FirebaseError.swift
//  Share My Journey
//
//  Created by Robert John Britton on 05/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit
import Firebase

struct FirebaseError {
    struct Auth {
        /// This method takes in an authentication error code retrieved from Firebase, then presents an altert based on the error value
        static func present(_ errorCode: AuthErrorCode, from viewController: UIViewController) -> Void {

            switch errorCode {
            case .invalidEmail:
                viewController.presentAlert(title: nil, message: Global.alertMsg.invalidEmail)
            case .wrongPassword:
                viewController.presentAlert(title: nil, message: Global.alertMsg.invalidEmailPassword)
            case .userNotFound:
                viewController.presentAlert(title: nil, message: Global.alertMsg.userNotFound)
            case .tooManyRequests:
                viewController.presentAlert(title: nil, message: Global.alertMsg.tooManyRequests)
            case .userDisabled:
                viewController.presentAlert(title: nil, message: Global.alertMsg.accountLocked)
            case .emailAlreadyInUse:
                viewController.presentAlert(title: nil, message: Global.alertMsg.emailAlreadyInUse)
            case .weakPassword:
                viewController.presentAlert(title: nil, message: Global.alertMsg.weakPassword)
            default:
                return
            }
        }
        
    }
}
