//
//  DropDownMenuButton.swift
//  Share My Journey
//
//  Created by Robert John Britton on 12/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//  Adapted from source:  https://www.youtube.com/watch?v=22zu-OTS-3M

import UIKit
import SnapKit


// A protocol that enables the Dropdown button to send the value it recieved from the dropdown button view along with the
// dropdown button's ID, to enable the identification of the button that holds the value.
protocol DropdownButtonDelegate {
    func valueForDropdownButton(_ value: String, withTag tag: Int)
}

// A protocol that the dropdown buttonView uses to pass the value of the selected item in the list.
protocol DropdownButtonViewDelegate {
    func valueForSelectedDropdownOption(value: String)
}

class DropdownButton: UIButton {
    
    public let defaultTag = Int.max
    
    
    public var dropdownMenuOptions = [String]() {
        didSet { self.dropdownView.dropdownOptions = dropdownMenuOptions }
    }
    
    
    // Defines the frame height for each option displayed in the dropdown menu
    public var dropdownMenuMaxHeight: CGFloat = 150.0
    
    
    public var delegate: DropdownButtonDelegate?
    
    
    private let dropdownView: DropdownButtonView!
    
    
    // Variable that keeps track of whether the dropdown view is open or closed
    private var dropdownViewIsOpen = false
    
    
    override init(frame: CGRect) {
        dropdownView = DropdownButtonView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        super.init(frame: frame)
        
        dropdownView.delegate = self
        self.tag = defaultTag
    }
    
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    
    // When the button is added to a view this method then adds the dropdown menu to that same view
    override func didMoveToSuperview() {
        self.superview?.addSubview(dropdownView)
        self.superview?.bringSubview(toFront: dropdownView)
    }
    
    
    // Set the constraints for the dropdown menu after it has been added to the button view (or the system will crash due to self.anchors not existing)
    override func didAddSubview(_ subview: UIView) {
        dropdownView.snp.makeConstraints({(make) in
            make.top.equalTo(self.snp.bottom)
            make.centerX.width.equalTo(self)
            make.height.equalTo(0)
        })
    }
    
   
    // Invoked when a user touches the button. Checks the state of the dropdown menu, if its closed, open it
    // if it open, close it, and set the state.
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if !dropdownViewIsOpen {
            
            dropdownViewIsOpen = true
            
            if self.dropdownView.tableView.contentSize.height > dropdownMenuMaxHeight {
                
                dropdownView.snp.updateConstraints({(make) in
                    make.height.equalTo(dropdownMenuMaxHeight)
                })
                
            } else {
                // Set the height of the dropdown options view equal to the table views content height (wraps the view height around
                // content when less than 150)
                dropdownView.snp.updateConstraints({(make) in
                    make.height.equalTo(self.dropdownView.tableView.contentSize.height)
                })
            }
            
            // Animates the opening of the dropdown options view
            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
                
                self.dropdownView.layoutIfNeeded()
                
                // Maintains the vertical alignment as the dropdown springs open
                self.dropdownView.center.y += self.dropdownView.frame.height / 2
                
            }, completion: nil)
        } else {
            self.dismissDropdownButtonView()
        }
    }
    
    // Sets the isOpen property to false then updates the dropdon menus' height contraint setting it to 0 making it disappear.
    // This is animated for visual appeal. Otherwise the dropown will instantly disappear.
    private func dismissDropdownButtonView() {
        dropdownViewIsOpen = false
        
        // Sets the dropdown option view heigh to 0
        dropdownView.snp.updateConstraints({(make) in
            make.height.equalTo(0)
        })
        
        // Animates the cloasure of the dropdown options view
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
            
            // Animates the dropdown menu closing from bottom to top.
            self.dropdownView.center.y -= self.dropdownView.frame.height / 2
            self.dropdownView.layoutIfNeeded()
        }, completion: nil)
    }
}

extension DropdownButton: DropdownButtonViewDelegate {
    // The method called when an item is selected from the dropdown menu
    func valueForSelectedDropdownOption(value: String) {
        self.setTitle(value, for: .normal)
        self.dismissDropdownButtonView()
        
        // Sends the value of the selected item to a registered view controller
        self.delegate?.valueForDropdownButton(value, withTag: self.tag)
    }
}






// The table view that displays the options for the dropdown button
class DropdownButtonView: UIView, UITableViewDelegate, UITableViewDataSource {

    // Sets the dropdown options to display, then reloads the table view
    public var dropdownOptions = [String]() {
        didSet { tableView.reloadData() }
    }


    // Initialise a tableView
    public let tableView: UITableView = UITableView()


    // The delegate invoked when an option is selected
    public var delegate: DropdownButtonViewDelegate?


    // Set the delegates to self, as we will manage the tableviews data
    override init(frame: CGRect) {
        super.init(frame: frame)

        tableView.delegate = self
        tableView.dataSource = self
    }


    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }


    override func didMoveToSuperview() {
        setupTableView()
    }


    // Sets up the view when its added as a subview of the dropdownButton
    private func setupTableView() {
        addSubview(tableView)

        // Set the tableview to fill the parent view (self)
        tableView.snp.makeConstraints({(make) in
            make.edges.equalToSuperview()
        })
    }


    // Sets the number of sections in the table, we only need 1
    func numberOfSections(in tableView: UITableView) -> Int { return 1 }


    // Sets the number of rows in the section, this is the number of options to display, which is calculated by counting the options array
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return dropdownOptions.count }


    // Defines the cell to display for each row, here we only need a default table view cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = dropdownOptions[indexPath.row]
        return cell
    }


    // Sets the height for each row
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }


    // Invoke the delegate and pass along the selected option. Deselect the row once call.
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.valueForSelectedDropdownOption(value: dropdownOptions[indexPath.row])
        self.tableView.deselectRow(at: indexPath, animated: true)
    }

}
