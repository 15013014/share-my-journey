//
//  AddJourneyNameView.swift
//  Share My Journey
//
//  Created by Robert John Britton on 18/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit

class AddJourneyNameView: UIView {

    public var continueButtonDelegate: AddJourneyLocationSelectionViewDelegate?
    
    var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hex: Global.colour.darkGrey)
        label.font = UIFont.systemFont(ofSize: 18, weight: UIFont.Weight.bold)
        label.numberOfLines = 1
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    var subTitleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.light)
        label.numberOfLines = 4
        label.lineBreakMode = .byTruncatingTail
        label.adjustsFontSizeToFitWidth = false
        return label
    }()
    
    var errorLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hex: Global.colour.red)
        label.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.bold)
        label.numberOfLines = 2
        label.lineBreakMode = .byTruncatingTail
        label.textAlignment = .center
        label.isHidden = true
        label.adjustsFontSizeToFitWidth = false
        return label
    }()
    
    let textField: BaseTextField = {
        let textField = RoundedTextField()
        textField.characterLimit = 20
        textField.validCharacters = Global.validCharSet.textNumericWithSpace
        return textField
    }()
    
    var continueButton: UIButton = {
        let button = UIButton() .defaultButton(title: Global.titleText.button.Continue)
        button.addTarget(self, action: #selector(handleContinueButtonTouch), for: .touchUpInside)
        return button
    }()
    
    
    override func didMoveToSuperview() {
        setupViews()
    }
    
    
    private func setupViews() {
        addSubview(titleLabel)
        addSubview(subTitleLabel)
        addSubview(errorLabel)
        addSubview(textField)
        addSubview(continueButton)
        
        
        titleLabel.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(safeAreaLayoutGuide).offset(64)
            make.centerX.equalToSuperview()
            make.left.equalTo(safeAreaLayoutGuide).offset(16)
        })
        
        subTitleLabel.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(titleLabel.snp.bottom).offset(8)
            make.centerX.equalToSuperview()
            make.left.equalToSuperview().offset(16)
            make.right.equalTo(titleLabel.snp.right)
        })
        
        // Displays the error label above the inputs
        errorLabel.snp.makeConstraints({(make) -> Void in
            make.bottom.equalTo(textField.snp.top).offset(-8)
            make.centerX.equalToSuperview()
            make.width.equalTo(textField.snp.width)
        })
        
        textField.snp.makeConstraints({(make) -> Void in
            make.centerX.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.height.equalTo(50)
        })
        
        continueButton.snp.makeConstraints({(make) -> Void in
            make.centerX.equalToSuperview()
            make.width.equalTo(textField.snp.width)
            make.height.equalTo(40)
            make.bottom.equalTo(safeAreaLayoutGuide).offset(-16)
        })
        
    }
    
    @objc private func handleContinueButtonTouch() {
        self.continueButtonDelegate?.handleContinueButtonTouch()
    }
    
}
