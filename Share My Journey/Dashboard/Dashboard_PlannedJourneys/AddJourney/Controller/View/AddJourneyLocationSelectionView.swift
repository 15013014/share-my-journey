//
//  JourneyLocationView.swift
//  Share My Journey
//
//  Created by Robert John Britton on 12/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit


protocol AddJourneyLocationSelectionViewDelegate {
    func handleContinueButtonTouch()
}


/// An interfcae that contains a pair of dropdown buttons for displaying choices
/// of cities and areas. This view requires a minimum height of 230 which will
/// allow for the drop down areas to expand and stay within the bounds of its view.
/// Property observers e.g. didSet are used to set the parameters for the dropdown
/// buttons whenever a value is changed.
class AddJourneyLocationSelectionView: UIView {
    
    /// Set the delegate that the city dropdown button will call when an area is selected
    public var cityDropdownButtonDelegate: DropdownButtonDelegate? = nil {
        didSet { cityDropdownButton.delegate = cityDropdownButtonDelegate }
    }
    
    /// Set the delegate that the area dropdown button will call when an area is selected
    public var areaDropdownButtonDelegate: DropdownButtonDelegate? = nil {
        didSet { areaDropdownButton.delegate = areaDropdownButtonDelegate }
    }
    
    public var continueButtonDelegate: AddJourneyLocationSelectionViewDelegate?
    
    /// Set the tag used to identify the city dropdown button
    public var cityButtonTag: Int? = nil {
        didSet {
            if let tag = cityButtonTag { cityDropdownButton.tag = tag }
        }
    }
    
    /// Set the tag used to identify the area dropdown button
    public var areaButtonTag: Int? = nil {
        didSet {
            if let tag = areaButtonTag { areaDropdownButton.tag = tag }
        }
    }
    
    /// The default text to display in the city dropdown button
    public let cityButtonDefaultTitle: String = "Select City"
    
    /// The default text to display in the area dropdown button
    public let areaButtonDefaultTitle: String = "Select Area"
    
    
    
    /// Set the list of cities the city dropdown button will display
    public var cities: [String] = [String]() {
        didSet { cityDropdownButton.dropdownMenuOptions = cities }
    }
    
    /// Set the list of areas the area dropdown button will display
    public var areas: [String] = [String]() {
        didSet { areaDropdownButton.dropdownMenuOptions = areas }
    }
    
    
    var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hex: Global.colour.darkGrey)
        label.font = UIFont.systemFont(ofSize: 18, weight: UIFont.Weight.bold)
        label.numberOfLines = 1
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    var subTitleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.light)
        label.numberOfLines = 4
        label.lineBreakMode = .byTruncatingTail
        label.adjustsFontSizeToFitWidth = false
        return label
    }()
    
    
    var errorLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hex: Global.colour.red)
        label.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.bold)
        label.numberOfLines = 2
        label.lineBreakMode = .byTruncatingTail
        label.textAlignment = .center
        label.isHidden = true
        label.adjustsFontSizeToFitWidth = false
        return label
    }()
    
    /// The city dropdown button that displays displays a list of cities that the user can select.
    /// Upon making a selection, the buttons' title text changes to reflect the choice made.
    private lazy var cityDropdownButton: DropdownButton = {
        let button = DropdownButton()
        button.setTitle(self.cityButtonDefaultTitle , for: .normal)
        button.setTitleColor(UIColor(hex: Global.colour.darkGrey), for: .normal)
        button.layer.borderColor = UIColor(hex: Global.colour.lightGrey).cgColor
        button.layer.cornerRadius = 8.0
        button.layer.borderWidth = 1.0
        button.clipsToBounds = true
        return button
    }()
    
    /// The area dropdown button that displays displays a list of areas that the user can select.
    /// Upon making a selection, the buttons' title text changes to reflect the choice made.
    private lazy var areaDropdownButton: DropdownButton = {
        let button = DropdownButton()
        button.setTitle(self.areaButtonDefaultTitle , for: .normal)
        button.setTitleColor(UIColor(hex: Global.colour.darkGrey), for: .normal)
        button.layer.borderColor = UIColor(hex: Global.colour.lightGrey).cgColor
        button.layer.cornerRadius = 8.0
        button.layer.borderWidth = 1.0
        button.clipsToBounds = true
        return button
    }()
    
    var continueButton: UIButton = {
        let button = UIButton() .defaultButton(title: Global.titleText.button.Continue)
        button.addTarget(self, action: #selector(handleContinueButtonTouch), for: .touchUpInside)
        return button
    }()
    
    
    // Set up the view once it has been added to a container view (e.g. a UIViewControllers' view)
    override func didMoveToSuperview() {
        setupView()
        
    }
    
    // Adds the dropdown buttons to the view and uses SnapKit to configure layout constraints.
    // The area is added to the view first so the city dropdown menu exapnds over the top of it.
    private func setupView() {
        addSubview(titleLabel)
        addSubview(subTitleLabel)
        addSubview(errorLabel)
        addSubview(areaDropdownButton)
        addSubview(cityDropdownButton)
        addSubview(continueButton)

        
        titleLabel.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(safeAreaLayoutGuide).offset(64)
            make.centerX.equalToSuperview()
            make.left.equalTo(safeAreaLayoutGuide).offset(16)
        })
        
        subTitleLabel.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(titleLabel.snp.bottom).offset(8)
            make.centerX.equalToSuperview()
            make.left.equalToSuperview().offset(16)
            make.right.equalTo(titleLabel.snp.right)
        })
        
        // Displays the error label above the inputs
        errorLabel.snp.makeConstraints({(make) -> Void in
            make.bottom.equalTo(cityDropdownButton.snp.top).offset(-8)
            make.centerX.equalToSuperview()
            make.width.equalTo(cityDropdownButton.snp.width)
        })
        
        cityDropdownButton.snp.makeConstraints({(make) -> Void in
            make.centerX.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.height.equalTo(50)
        })
        
        areaDropdownButton.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(cityDropdownButton.snp.bottom).offset(8)
            make.centerX.equalToSuperview()
            make.width.equalTo(cityDropdownButton.snp.width)
            make.height.equalTo(50)
        })
        
        continueButton.snp.makeConstraints({(make) -> Void in
            make.centerX.equalToSuperview()
            make.width.equalTo(cityDropdownButton.snp.width)
            make.height.equalTo(40)
            make.bottom.equalTo(safeAreaLayoutGuide).offset(-16)
        })
        
        
    }
    
    public func setCityButtonText(text: String?) {
        self.cityDropdownButton.setTitle(text, for: .normal)
    }
    
    
    public func setAreaButtonText(text: String?) {
        self.areaDropdownButton.setTitle(text, for: .normal)
    }
    
    public func getAreaButtonText() -> String {
        if let text = self.areaDropdownButton.titleLabel?.text {
            return text
        }
        return areaButtonDefaultTitle
    }
    
    public func getCityButtonText() -> String {
        if let text = self.cityDropdownButton.titleLabel?.text {
            return text
        }
        return cityButtonDefaultTitle
    }

    
    @objc private func handleContinueButtonTouch() {
        self.continueButtonDelegate?.handleContinueButtonTouch()
    }

}
