//
//  AddJourneyVehicleSelectionView.swift
//  Share My Journey
//
//  Created by Robert John Britton on 17/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit

class AddJourneyVehicleSelectionView: UIView {

    /// Set the delegate that the city dropdown button will call when an area is selected
    public var vehicleDropdownButtonDelegate: DropdownButtonDelegate? = nil {
        didSet { vehicleDropdownButton.delegate = vehicleDropdownButtonDelegate }
    }
    
    public var continueButtonDelegate: AddJourneyLocationSelectionViewDelegate?
    
    /// Set the list of cities the city dropdown button will display
    public var vehicles: [String] = [String]() {
        didSet { vehicleDropdownButton.dropdownMenuOptions = vehicles }
    }
    
    /// The default text to display in the city dropdown button
    public let vehicleButtonDefaultTitle: String = "Select Vehicle"
    
    var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hex: Global.colour.darkGrey)
        label.font = UIFont.systemFont(ofSize: 18, weight: UIFont.Weight.bold)
        label.numberOfLines = 1
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    var subTitleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.light)
        label.numberOfLines = 4
        label.lineBreakMode = .byTruncatingTail
        label.adjustsFontSizeToFitWidth = false
        return label
    }()
    
    var errorLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hex: Global.colour.red)
        label.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.bold)
        label.numberOfLines = 2
        label.lineBreakMode = .byTruncatingTail
        label.textAlignment = .center
        label.isHidden = true
        label.adjustsFontSizeToFitWidth = false
        return label
    }()
    
    
    /// The city dropdown button that displays displays a list of cities that the user can select.
    /// Upon making a selection, the buttons' title text changes to reflect the choice made.
    private lazy var vehicleDropdownButton: DropdownButton = {
        let button = DropdownButton()
        button.setTitle(self.vehicleButtonDefaultTitle , for: .normal)
        button.setTitleColor(UIColor(hex: Global.colour.darkGrey), for: .normal)
        button.layer.borderColor = UIColor(hex: Global.colour.lightGrey).cgColor
        button.layer.cornerRadius = 8.0
        button.layer.borderWidth = 1.0
        button.clipsToBounds = true
        return button
    }()
    
    
    var continueButton: UIButton = {
        let button = UIButton() .defaultButton(title: Global.titleText.button.Continue)
        button.addTarget(self, action: #selector(handleContinueButtonTouch), for: .touchUpInside)
        return button
    }()
    
    override func didMoveToSuperview() {
        setupViews()
    }
    
    
    private func setupViews() {
        
        
        addSubview(titleLabel)
        addSubview(subTitleLabel)
        addSubview(errorLabel)
        addSubview(vehicleDropdownButton)
        addSubview(continueButton)
        
        
        titleLabel.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(safeAreaLayoutGuide).offset(64)
            make.centerX.equalToSuperview()
            make.left.equalTo(safeAreaLayoutGuide).offset(16)
        })
        
        subTitleLabel.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(titleLabel.snp.bottom).offset(8)
            make.centerX.equalToSuperview()
            make.left.equalToSuperview().offset(16)
            make.right.equalTo(titleLabel.snp.right)
        })
        
        // Displays the error label above the inputs
        errorLabel.snp.makeConstraints({(make) -> Void in
            make.bottom.equalTo(vehicleDropdownButton.snp.top).offset(-8)
            make.centerX.equalToSuperview()
            make.width.equalTo(vehicleDropdownButton.snp.width)
        })
        
        vehicleDropdownButton.snp.makeConstraints({(make) -> Void in
            make.centerX.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.height.equalTo(50)
        })
        
        continueButton.snp.makeConstraints({(make) -> Void in
            make.centerX.equalToSuperview()
            make.width.equalTo(vehicleDropdownButton.snp.width)
            make.height.equalTo(40)
            make.bottom.equalTo(safeAreaLayoutGuide).offset(-16)
        })
        
        
    }
    
    @objc private func handleContinueButtonTouch() {
        self.continueButtonDelegate?.handleContinueButtonTouch()
    }
    
    
    public func getVehicleButtonText() -> String {
        if let text = self.vehicleDropdownButton.titleLabel?.text {
            return text
        }
        return vehicleButtonDefaultTitle
    }

}
