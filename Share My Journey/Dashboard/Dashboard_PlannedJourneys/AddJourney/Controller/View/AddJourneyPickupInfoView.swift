//
//  AddJourneyPickupInfoView.swift
//  Share My Journey
//
//  Created by Robert John Britton on 18/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit

protocol AddJourneyAddJourneyPickupInfoViewDelegate {
    func handleTextViewToolbarDoneButtonTouch()
}

class AddJourneyPickupInfoView: UIView {

    public var continueButtonDelegate: AddJourneyLocationSelectionViewDelegate?
    public var textViewToolbarDelegate: AddJourneyAddJourneyPickupInfoViewDelegate?
    
    var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hex: Global.colour.darkGrey)
        label.font = UIFont.systemFont(ofSize: 18, weight: UIFont.Weight.bold)
        label.numberOfLines = 1
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    var subTitleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.light)
        label.numberOfLines = 4
        label.lineBreakMode = .byTruncatingTail
        label.adjustsFontSizeToFitWidth = false
        return label
    }()
    
    var errorLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hex: Global.colour.red)
        label.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.bold)
        label.numberOfLines = 2
        label.lineBreakMode = .byTruncatingTail
        label.textAlignment = .center
        label.isHidden = true
        label.adjustsFontSizeToFitWidth = false
        return label
    }()
    
    lazy var textView: UITextView = {
        let textView = UITextView()
        textView.backgroundColor = UIColor(hex: Global.colour.white)
        textView.layer.cornerRadius = 10
        textView.layer.borderWidth = 1
        textView.layer.borderColor = UIColor(hex: Global.colour.lightGrey).cgColor
        textView.clipsToBounds = true
        textView.font = UIFont.systemFont(ofSize: 14)
        let contentInstets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        textView.contentInset = contentInstets
        textView.returnKeyType = .default
        textView.textContainer.maximumNumberOfLines = 10
        textView.inputAccessoryView = self.textViewToolbar
        return textView
    }()
    
    let textViewToolbar: UIToolbar = {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let flexSpaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let donButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(textFieldToolBarDoneButtonTouched))
        toolbar.setItems([flexSpaceButton, flexSpaceButton, donButton], animated: false)
        toolbar.isUserInteractionEnabled = true
        return toolbar
    }()
    
    var continueButton: UIButton = {
        let button = UIButton() .defaultButton(title: Global.titleText.button.Continue)
        button.addTarget(self, action: #selector(handleContinueButtonTouch), for: .touchUpInside)
        return button
    }()
    
    
    override func didMoveToSuperview() {
        setupViews()
    }
    
    
    private func setupViews() {
        addSubview(titleLabel)
        addSubview(subTitleLabel)
        addSubview(errorLabel)
        addSubview(textView)
        addSubview(continueButton)
        
        
        titleLabel.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(safeAreaLayoutGuide).offset(64)
            make.centerX.equalToSuperview()
            make.left.equalTo(safeAreaLayoutGuide).offset(16)
        })
        
        subTitleLabel.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(titleLabel.snp.bottom).offset(8)
            make.centerX.equalToSuperview()
            make.left.equalToSuperview().offset(16)
            make.right.equalTo(titleLabel.snp.right)
        })
        
        // Displays the error label above the inputs
        errorLabel.snp.makeConstraints({(make) -> Void in
            make.bottom.equalTo(textView.snp.top).offset(-8)
            make.centerX.equalToSuperview()
            make.width.equalTo(textView.snp.width)
        })
        
        textView.snp.makeConstraints({(make) -> Void in
            make.centerX.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.height.equalTo(100)
        })
        
        continueButton.snp.makeConstraints({(make) -> Void in
            make.centerX.equalToSuperview()
            make.width.equalTo(textView.snp.width)
            make.height.equalTo(40)
            make.bottom.equalTo(safeAreaLayoutGuide).offset(-16)
        })
        
    }
    
    @objc private func handleContinueButtonTouch() {
        self.continueButtonDelegate?.handleContinueButtonTouch()
    }
    
    @objc private func textFieldToolBarDoneButtonTouched() {
        self.textViewToolbarDelegate?.handleTextViewToolbarDoneButtonTouch()
    }

}
