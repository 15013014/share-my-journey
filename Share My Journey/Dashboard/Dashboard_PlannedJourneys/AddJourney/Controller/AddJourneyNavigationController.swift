//
//  AddJourneyNavigationController.swift
//  Share My Journey
//
//  Created by Robert John Britton on 17/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit
import Firebase

class AddJourneyNavigationController: NavigationControllerLight {
    public var userJourney = UserJourney()
    public var userVehicles = [UserVehicle]()

    var selectedVehicle: UserVehicle?
    // The vehicle the user selects will have it's index stored here, so we can quickly reference the vehicle's information stroed in the userVehicles array

    override func viewDidLoad() {
        fetchVehicles()
    }
    
    
    // Fetch the vehicles the user owns as soon as the navigation loads so that they are avaiable when the user needs to select one.
    private func fetchVehicles() {
        // Get the initial vehicles that the user owns when the view loads (this happens once)
        if let userId = Auth.auth().currentUser?.uid {
            
            // Set the driverId as the users' Id in the userJourney model
            self.userJourney.driverId = userId
            
            let userVehiclesRef = Database.database().reference().child("user_vehicles/\(userId)")
            
            // observes changes to the user vehicles, pulling references from the node som we can find the
            // vehicle information in the vehicles node
            userVehiclesRef.observeSingleEvent(of: .value, with: {(snapshot) in

                for child in snapshot.children {
                    let snap = child as! DataSnapshot
                    let vehicleKey = snap.key
                    
                    let vehiclesRef = Database.database().reference().child("vehicles/\(vehicleKey)")
                    
                    // Pulls vehicle information, whic is identified by the reference from the query above
                    // Creates a user vehicle model which is added to a temp array. Once all vehicles have been
                    // pulled, set the vehicle array in view controller, which reloads the collection view
                    vehiclesRef.observeSingleEvent(of: .value, with: {(vehicleSnapshot) in

                        // Create a dictionary to store the key:value JSon data from database
                        if let dictionary = vehicleSnapshot.value as? [String: AnyObject] {
                            
                            let userVehicle = UserVehicle()
                            userVehicle.colour = dictionary["colour"] as! String
                            userVehicle.make = dictionary["make"] as! String
                            userVehicle.model = dictionary["model"] as! String
                            userVehicle.passengerSeats = dictionary["passengerSeats"] as! Int
                            userVehicle.registration = dictionary["registration"] as! String
                            userVehicle.userId = dictionary["userId"] as! String
                            userVehicle.vehicleId = dictionary["vehicleId"] as! String
                            
                            // Append each new vehicle object the array
                            self.userVehicles.append(userVehicle)
                        }
                    })
                }
            })
        }
    }
    
    
    public func createJourney() {
        
        // the journey information will exist in 1 node, whilst references to the journey will exist in 3
        
        let ref = Database.database().reference()
        let journeysRef = ref.child("journeys")
        let userJourneysRef = ref.child("user_journeys")
        let journeyStartCityAreaRef = ref.child("journey_start_city_area")
        //let journeyEndCityAreaRef = ref.child("journey_end_city_area")
        
        // Create a new child within the journeys node and store the key, this will be the journeys' ID
        let journeyId = journeysRef.childByAutoId().key
        userJourney.journeyId = journeyId
        
        // Format the userJourney model so it's accepted by Firebase
        
        // We know that all properties in the journey object have been checked by the view controllers, so we can safely assume the object properties contain a value.
        // Therefore we can force unwrap the contents of each property
        
        let post: Any = [
            "journeyName":"\(userJourney.journeyName!)",
            "journeyId":"\(journeyId)",
            "startCity":"\(userJourney.startCity!)",
            "startArea":"\(userJourney.startArea!)",
            "startDate":userJourney.startDate!,
            "endCity":"\(userJourney.endCity!)",
            "endArea":"\(userJourney.endArea!)",
            "endDate":userJourney.endDate!,
            "driverId":"\(userJourney.driverId!)",
            "vehicleId":"\(userJourney.vehicleId!)",
            "maxPassengers":userJourney.maxPassengers!,
            "pickupInfo":"\(userJourney.pickupInfo!)"
        ];
        
        // Put the journey into the journeys node
        journeysRef.child(journeyId).setValue(post)
        
        // Create a reference between the journey creator (current user / driver) in a seperate node
        // Give this reference a value that is the journeys end date. This can then be used to speed up finding out when the journey has been completed.
        userJourneysRef.child("\(userJourney.driverId!)/\(journeyId)").setValue(userJourney.endDate!)
        
        // Create a reference to the journey in another node used for querying possible matches
        journeyStartCityAreaRef.child("\(userJourney.startCity!)_\(userJourney.startArea!)/\(journeyId)").setValue("\(userJourney.driverId!)")
        
        // Create a reference to the journey in another node to quickly assume if the journey is a match when it comes to querying later.
        //journeyEndCityAreaRef.child("\(userJourney.endCity!)_\(userJourney.endArea!)/\(journeyId)").setValue("\(userJourney.driverId!)")
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
