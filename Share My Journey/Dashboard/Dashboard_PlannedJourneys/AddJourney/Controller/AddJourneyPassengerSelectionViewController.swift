//
//  AddVehiclePassengerSelectionViewController.swift
//  Share My Journey
//
//  Created by Robert John Britton on 17/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit

class AddJourneyPassengerSelectionViewController: UIViewController, AddJourneyLocationSelectionViewDelegate, DropdownButtonDelegate{
    
    // The vehicle detials to display in the dropdown menu. We will display: 'Vehicle Make, Registration'
    var passengers = [String]() {
        didSet { passengerSelectionView.passengers = passengers }
    }
    
    private lazy var passengerSelectionView: AddJourneyPassengerSelectionView = {
        let view = AddJourneyPassengerSelectionView()
        view.titleLabel.text = "How Many Passengers Can Travel With You?"
        view.subTitleLabel.text = "The number of passengers you select cannot exceed the passenger seats specified for the vehicle chosen"
        view.errorLabel.text = "Error: You Must Select a Number of Passengers"
        view.continueButtonDelegate = self
        view.passengerDropdownButtonDelegate = self
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.white
        
        navigationItem.title = "Your Journey Details"
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(handleNavigationBackButtonTouch))
        
        getPassengerDetails()
        setupView()
    }
    
    
    private func setupView() {
        view.addSubview(passengerSelectionView)
        
        passengerSelectionView.snp.makeConstraints({(make) -> Void in
            make.edges.equalToSuperview()
        })
    }
    
    
    private func getPassengerDetails() {
        guard let navigationController = (navigationController as? AddJourneyNavigationController) else { return }
        
        if navigationController.selectedVehicle != nil {
            let maxNumOfPassengers = navigationController.selectedVehicle!.passengerSeats
            
            for index in 1...maxNumOfPassengers {
                passengers.append(String(index))
            }
        }
    }
    
    @objc private func handleNavigationBackButtonTouch() {
        navigationController?.popViewController(animated: true)
    }
    
    
    func handleContinueButtonTouch() {
        guard let navigationController = (navigationController as? AddJourneyNavigationController) else { return }
        
        let selectedPassengerTitle = passengerSelectionView.getPassengerButtonText()
        
        if selectedPassengerTitle == passengerSelectionView.passengerButtonDefaultTitle {
            passengerSelectionView.errorLabel.isHidden = false
            return
        }
        
        navigationController.userJourney.maxPassengers = Int(selectedPassengerTitle)
        
        navigationController.pushViewController(AddJourneyPickupInfoViewController(), animated: true)
    }
    
    func valueForDropdownButton(_ value: String, withTag tag: Int) {
        passengerSelectionView.errorLabel.isHidden = true
    }
}
