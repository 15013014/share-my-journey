//
//  AddJourneyPickupInfoViewController.swift
//  Share My Journey
//
//  Created by Robert John Britton on 18/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit

class AddJourneyPickupInfoViewController: UIViewController, AddJourneyLocationSelectionViewDelegate, AddJourneyAddJourneyPickupInfoViewDelegate{
    
    private lazy var journeyPickeupInfoView: AddJourneyPickupInfoView = {
        let view = AddJourneyPickupInfoView()
        view.titleLabel.text = "Please Provide Additional Journey Pickup Information"
        view.subTitleLabel.text = "You should specify a pickup time and suitable location so passengers know where to meet up"
        view.errorLabel.text = "Error: You Must Provide Additional Pickup Information"
        view.continueButtonDelegate = self
        view.textViewToolbarDelegate = self
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.white
        
        navigationItem.title = "Your Journey Details"
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(handleNavigationBackButtonTouch))
        
        setupView()
    }
    
    private func setupView() {
        view.addSubview(journeyPickeupInfoView)
        
        journeyPickeupInfoView.snp.makeConstraints({(make) -> Void in
            make.edges.equalToSuperview()
        })
    }
    
    
    // Dismiss the view controller if the user taps the back button on the navigation bar
    @objc private func handleNavigationBackButtonTouch() {
        navigationController?.popViewController(animated: true)
    }
    
    // Check that the user has provided information in the textfield. Display error if textfield is empty and return out.
    //Process the userJourney model, adding it to the database
    func handleContinueButtonTouch() {
        guard let navigationController = (navigationController as? AddJourneyNavigationController) else { return }
        
        if journeyPickeupInfoView.textView.text!.isEmpty {
            journeyPickeupInfoView.errorLabel.isHidden = false
            return
        }
        
        journeyPickeupInfoView.errorLabel.isHidden = true
        
        navigationController.userJourney.pickupInfo = journeyPickeupInfoView.textView.text!
        
        navigationController.pushViewController(AddJourneyNameViewController(), animated: true)
    }
    
    func handleTextViewToolbarDoneButtonTouch() {
        if !journeyPickeupInfoView.textView.text!.isEmpty {
            journeyPickeupInfoView.errorLabel.isHidden = true
            journeyPickeupInfoView.textView.resignFirstResponder()
            return
        }
        
        journeyPickeupInfoView.errorLabel.isHidden = false
    }
    
}
