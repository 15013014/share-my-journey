//
//  AddJourneyNameViewController.swift
//  Share My Journey
//
//  Created by Robert John Britton on 18/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit

class AddJourneyNameViewController: UIViewController, AddJourneyLocationSelectionViewDelegate, UITextFieldDelegate{
    
    private lazy var journeyNameView: AddJourneyNameView = {
        let view = AddJourneyNameView()
        view.titleLabel.text = "Finally, Give Your Journey a Name"
        view.subTitleLabel.text = "Naming your journey will help you to identify it later on\nNote: This name will only be visible to you."
        view.textField.placeholder = "Journey Name..."
        view.errorLabel.text = "Error: You Must Provide Your Journey With a Name"
        view.continueButtonDelegate = self
        view.textField.delegate = self
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.white
        
        navigationItem.title = "Your Journey Details"
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(handleNavigationBackButtonTouch))
        
        setupView()
    }
    
    private func setupView() {
        view.addSubview(journeyNameView)
        
        journeyNameView.snp.makeConstraints({(make) -> Void in
            make.edges.equalToSuperview()
        })
    }
    

    // Dismiss the view controller if the user taps the back button on the navigation bar
    @objc private func handleNavigationBackButtonTouch() {
        navigationController?.popViewController(animated: true)
    }
    
    // Check that the user has provided information in the textfield. Display error if textfield is empty and return out.
    //Process the userJourney model, adding it to the database
    func handleContinueButtonTouch() {
        if journeyNameView.textField.text!.isEmpty {
            journeyNameView.errorLabel.isHidden = false
            return
        }
        
        journeyNameView.continueButton.isEnabled = false
        
        journeyNameView.errorLabel.isHidden = true
        
        guard let navigationController = (navigationController as? AddJourneyNavigationController) else { return }
        
        navigationController.userJourney.journeyName = journeyNameView.textField.text!
        
        // Tells the navigation controller to create the journey. The journey is added to the database
        navigationController.createJourney()
        
        dismiss(animated: true, completion: nil)
        
    }
    
    
    // Hide the error message as the user begins to edit the text field
    func textFieldDidBeginEditing(_ textField: UITextField) {
        journeyNameView.errorLabel.isHidden = true
    }
    
    // Hide the keyboard when the user taps the return key
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if !journeyNameView.textField.text!.isEmpty {
            journeyNameView.errorLabel.isHidden = true
            textField.resignFirstResponder()
            return true
        }
        
        journeyNameView.errorLabel.isHidden = false
        return false
    }
}
