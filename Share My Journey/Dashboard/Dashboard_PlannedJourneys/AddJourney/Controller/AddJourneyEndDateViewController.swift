//
//  AddJourneyEndTimeViewController.swift
//  Share My Journey
//
//  Created by Robert John Britton on 17/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit

class AddJourneyEndDateViewController: UIViewController, AddJourneyLocationSelectionViewDelegate {
    
    private lazy var endDateView: AddJourneyDateSelectionView = {
        let view = AddJourneyDateSelectionView()
        view.titleLabel.text = "What Time Will You Arrive?"
        view.continueButtonDelegate = self
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.white
        
        navigationItem.title = "Your Journey Details"
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(handleNavigationBackButtonTouch))
        
        setMinimumEndDate()
        setupView()
    }
    
    private func setupView() {
        view.addSubview(endDateView)
        
        endDateView.snp.makeConstraints({(make) -> Void in
            make.edges.equalToSuperview()
        })
    }
    
    /// Sets the initial date to the date start date stored in the userModel (which was set by the previous viewController)
    /// This prevents the user from selecting an arrival date to be earlier than the leaving date
    private func setMinimumEndDate() {
        guard let navigationController = (navigationController as? AddJourneyNavigationController) else { return }
        
        if let startDateTimeInterval = navigationController.userJourney.startDate {
            
            let minDate = Date(timeIntervalSince1970: TimeInterval(startDateTimeInterval))
            
            endDateView.datePickerView.minimumDate = minDate
            endDateView.datePickerView.date = minDate
            
            
            // Find the maximum allowed date from the minimum (a user may only select an ariavl date which is 24 hours in front of the start date)
            let date = minDate
            var dateComponents = DateComponents()
            dateComponents.hour = 24
            let calenderGregorian = Calendar(identifier: .gregorian)
            let maxSelectableDate = calenderGregorian.date(byAdding: dateComponents, to: date)
            
            endDateView.datePickerView.maximumDate = maxSelectableDate
        }
    }
    
    @objc private func handleNavigationBackButtonTouch() {
        navigationController?.popViewController(animated: true)
    }
    
    func handleContinueButtonTouch() {
        
        // Safely downcast the current navigation controller, to access the UserJourney model so we can store the arrival date.
        if let navigationController = (navigationController as? AddJourneyNavigationController) {
            let date = FormatDate.formateDateToIntegerTimeInterval(date: endDateView.datePickerView.date)
            navigationController.userJourney.endDate = date
            navigationController.pushViewController(AddJourneyVehicleSelectionViewController(), animated: true)
        } else { return }

    }
    
    
}
