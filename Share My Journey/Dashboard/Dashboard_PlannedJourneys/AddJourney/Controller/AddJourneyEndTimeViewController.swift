//
//  AddJourneyEndTimeViewController.swift
//  Share My Journey
//
//  Created by Robert John Britton on 17/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit

class AddJourneyEndDateViewController: UIViewController, AddJourneyLocationSelectionViewDelegate {
    
    var userJourney = UserJourney()
    
    let maxDate: Date = {
        let date = Date()
        var dateComponents = DateComponents()
        dateComponents.day = 7
        let calenderGregorian = Calendar(identifier: .gregorian)
        let maxSelectableDate = calenderGregorian.date(byAdding: dateComponents, to: date)
        return maxSelectableDate!
    }()
    
    
    private lazy var endDateView: AddJourneyDateSelectionView = {
        let view = AddJourneyDateSelectionView()
        view.titleLabel.text = "What Time Will You Arrive?"
        view.continueButtonDelegate = self
        view.datePickerView.maximumDate = maxDate
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.white
        
        navigationItem.title = "Your Journey Details"
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(handleNavigationBackButtonTouch))
        
        setMinimumEndDate()
        setupView()
    }
    
    private func setupView() {
        view.addSubview(endDateView)
        
        endDateView.snp.makeConstraints({(make) -> Void in
            make.edges.equalToSuperview()
        })
    }
    
    /// Sets the initial date to the date start date stored in the userModel (which was set by the previous viewController)
    /// This prevents the user from selecting an arrival date to be earlier than the leaving date
    private func setMinimumEndDate() {
        if let startDateTimeInterval = userJourney.startDate {
            let minDate = Date(timeIntervalSince1970: TimeInterval(startDateTimeInterval))
                endDateView.datePickerView.minimumDate = minDate
                endDateView.datePickerView.date = minDate
        }
    }
    
    @objc private func handleNavigationBackButtonTouch() {
        navigationController?.popViewController(animated: true)
    }
    
    func handleContinueButtonTouch() {
        let date = FormatDate.formateDateToIntegerTimeInterval(date: endDateView.datePickerView.date)
        
        userJourney.startDate = date
        
        let nextViewController = AddJourneyEndLocationViewController()
        nextViewController.userJourney = userJourney
        navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    
}
