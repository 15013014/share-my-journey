//
//  AddVehicleNavigationController.swift
//  Share My Journey
//
//  Created by Robert John Britton on 17/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit

class AddJourneyNavigationController: NavigationControllerLight {

    

    



}

extension AddJourneyNavigationController: DashboardDelegate {
    func updateUserProfile(_ userProfile: UserProfile) {
        <#code#>
    }
    
    func getUserProfile() -> UserProfile? {
        <#code#>
    }
    
    func updateUserVehicles(_ userVehicles: [UserVehicle]) {
        <#code#>
    }
    
    func getUserVehicles() -> [UserVehicle]? {
        <#code#>
    }
    
    func updateUserJourneys(_ userJourneys: [UserJourney]) {
        <#code#>
    }
    
    func getUserJourneys() -> [UserJourney]? {
        <#code#>
    }
}
