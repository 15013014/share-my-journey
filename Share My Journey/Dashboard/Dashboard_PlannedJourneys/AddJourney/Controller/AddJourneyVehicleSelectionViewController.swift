//
//  AddJourneyVehicleSelectionViewController.swift
//  Share My Journey
//
//  Created by Robert John Britton on 17/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit
import Firebase


class AddJourneyVehicleSelectionViewController: UIViewController, AddJourneyLocationSelectionViewDelegate, DropdownButtonDelegate{

    // The vehicle detials to display in the dropdown menu. We will display: 'Vehicle Make, Registration'
    var userVehicleDetails = [String]() {
        didSet { vehicleSelectionView.vehicles = userVehicleDetails }
    }
    
    var userVehicleIndex = Dictionary<String, Int>()
    
    private lazy var vehicleSelectionView: AddJourneyVehicleSelectionView = {
        let view = AddJourneyVehicleSelectionView()
        view.titleLabel.text = "Which Vehicle Will You Be Using?"
        view.subTitleLabel.text = "So potential passengers know what to look out for."
        view.errorLabel.text = "Error: You Must Select a Vehicle"
        view.continueButtonDelegate = self
        view.vehicleDropdownButtonDelegate = self
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.white
        
        navigationItem.title = "Your Journey Details"
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(handleNavigationBackButtonTouch))
        
        getVehicleDetails()
        setupView()
    }
    
    
    private func setupView() {
        view.addSubview(vehicleSelectionView)
        
        vehicleSelectionView.snp.makeConstraints({(make) -> Void in
            make.edges.equalToSuperview()
        })
    }
    
    
    private func getVehicleDetails() {
        guard let navigationController = (navigationController as? AddJourneyNavigationController) else { return }
        
        for (index, vehicle) in navigationController.userVehicles.enumerated() {
            let dropdownOption: String = "\(vehicle.make), \(vehicle.registration)"
            
            userVehicleDetails.append(dropdownOption)
            userVehicleIndex[dropdownOption] = index
        }
    }
    
    @objc private func handleNavigationBackButtonTouch() {
        navigationController?.popViewController(animated: true)
    }
    
    
    func handleContinueButtonTouch() {
        guard let navigationController = (navigationController as? AddJourneyNavigationController) else { return }
        
        let selectedVehicleTitle = vehicleSelectionView.getVehicleButtonText()
        
        if selectedVehicleTitle == vehicleSelectionView.vehicleButtonDefaultTitle {
            vehicleSelectionView.errorLabel.isHidden = false
            return
        }
        
        if let vehicleIndex = userVehicleIndex[selectedVehicleTitle] {
            navigationController.selectedVehicle = navigationController.userVehicles[vehicleIndex]
            navigationController.userJourney.vehicleId = navigationController.selectedVehicle?.vehicleId
            
//            print(navigationController.userJourney.vehicleId)
        } else {
            fatalError("Vehicle Index not set")
        }
        
        navigationController.pushViewController(AddJourneyPassengerSelectionViewController(), animated: true)
    }
    
    func valueForDropdownButton(_ value: String, withTag tag: Int) {
        vehicleSelectionView.errorLabel.isHidden = true
    }
}
