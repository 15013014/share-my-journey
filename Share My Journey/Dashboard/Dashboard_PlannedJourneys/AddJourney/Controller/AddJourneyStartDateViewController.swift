//
//  AddJourneyStartDateViewController.swift
//  Share My Journey
//
//  Created by Robert John Britton on 17/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit

class AddJourneyStartDateViewController: UIViewController, AddJourneyLocationSelectionViewDelegate {

    let minDate: Date = {
        let date = Date()
        return date
    }()
    
    let maxDate: Date = {
        let date = Date()
        var dateComponents = DateComponents()
        dateComponents.day = 7
        let calenderGregorian = Calendar(identifier: .gregorian)
        let maxSelectableDate = calenderGregorian.date(byAdding: dateComponents, to: date)
        return maxSelectableDate!
    }()
    
    
    private lazy var startDateView: AddJourneyDateSelectionView = {
        let view = AddJourneyDateSelectionView()
        view.titleLabel.text = "What Time Are You Leaving?"
        view.continueButtonDelegate = self
        view.datePickerView.minimumDate = minDate
        view.datePickerView.maximumDate = maxDate
        view.datePickerView.date = minDate
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.white
        
        navigationItem.title = "Your Journey Details"
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(handleNavigationBackButtonTouch))
        
        setupView()
    }
    
    private func setupView() {
        view.addSubview(startDateView)
        
        startDateView.snp.makeConstraints({(make) -> Void in
            make.edges.equalToSuperview()
        })
    }
    
    
    @objc private func handleNavigationBackButtonTouch() {
        navigationController?.popViewController(animated: true)
    }
    
    func handleContinueButtonTouch() {
        
        if let navigationController = (navigationController as? AddJourneyNavigationController) {
            let date = FormatDate.formateDateToIntegerTimeInterval(date: startDateView.datePickerView.date)
            navigationController.userJourney.startDate = date
            navigationController.pushViewController(AddJourneyEndLocationViewController(), animated: true)
        } else { return }
        
    }
}
