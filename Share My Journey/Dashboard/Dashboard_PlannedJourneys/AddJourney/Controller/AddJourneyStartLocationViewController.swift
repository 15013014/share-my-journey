//
//  AddJouneryStartLocationViewController.swift
//  Share My Journey
//
//  Created by Robert John Britton on 17/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit
import Firebase


class AddJourneyStartLocationViewController: UIViewController, AddJourneyLocationSelectionViewDelegate {

    
    
    var startCities = [String]() {
        didSet { startLocationView.cities = startCities }
    }
    
    var startAreas = [String]() {
        didSet { startLocationView.areas = startAreas }
    }
    
    private lazy var startLocationView: AddJourneyLocationSelectionView = {
        let view = AddJourneyLocationSelectionView()
        view.titleLabel.text = "Where Are You Leaving From?"
        view.subTitleLabel.text = "The selection is broad, but this gives you a chance to discus a pickup spot with potential passengers."
        view.cityDropdownButtonDelegate = self
        view.continueButtonDelegate = self
        return view
    }()
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nil, bundle: nil)
        
        loadCityData()
    }
        
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.white
        
        navigationItem.title = "Your Journey Details"
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(handleNavigationBackButtonTouch))
        
        setupViews()
    }
    
    private func setupViews() {
        view.addSubview(startLocationView)
        
        startLocationView.snp.makeConstraints({(make) -> Void in
            make.edges.equalToSuperview()
        })
        
    }
    
    
    /// Retrieves a list of cities pulled from the cities node in the database, setting the cities array
    private func loadCityData() {
        var cities = [String]()
        
        // Check if current user is authenticated before continueing
        if (Auth.auth().currentUser?.uid) != nil {
            
            // Create a reference to the root of cities node
            let citiesRef = Database.database().reference().child("cities")
            
            // Pull all values within cities node
            citiesRef.observeSingleEvent(of: .value, with: {(snapshot) in
                
                // Loop through key:values and obtain the values, downcast as string
                for child in snapshot.children {
                    
                    let snap = child as! DataSnapshot
                    
                    // We will present these values to the user to make a selection
                    let city = snap.value as! String
                    cities.append(city)
                }
                
                self.startCities = cities
            })
        }
    }
    
    
    @objc private func handleNavigationBackButtonTouch() {
        dismiss(animated: true, completion: nil)
    }
    
    func handleContinueButtonTouch() {
        let city = startLocationView.getCityButtonText()
        let area = startLocationView.getAreaButtonText()
        
        // check that the user has selected a city
        if city == startLocationView.cityButtonDefaultTitle {
            startLocationView.errorLabel.text = "Error: You must select a city."
            startLocationView.errorLabel.isHidden = false
            return
        }
        
        if area == startLocationView.areaButtonDefaultTitle {
            startLocationView.errorLabel.text = "Error: You must select an area."
            startLocationView.errorLabel.isHidden = false
            return
        }
        
        // if the above conditions are met, hide the error label
        startLocationView.errorLabel.isHidden = true
        
        if let navigationController = (navigationController as? AddJourneyNavigationController) {
            navigationController.userJourney.startCity = city
            navigationController.userJourney.startArea = area
        } else { return }
        
        navigationController?.pushViewController(AddJourneyStartDateViewController(), animated: true)
    }
}


extension AddJourneyStartLocationViewController: DropdownButtonDelegate {
    
    // This method is triggered whenever the user selects a city. The selected city string value is passed in.
    // we use this city to pull all the areas associated with the city from the database.
    // We then set the startAreas array, which produces a list of ares the user can choose from in the areas dropdown menu.
    // This ensures that the user cannot select an area that doesn't exist in a selected city.
    func valueForDropdownButton(_ value: String, withTag tag: Int) {
        // We only registered the cityButton delegate so we didnt need to set the button's tag. We know that the selected value coming in is from the city button.
        // Therefor we do not need to check the button's tag.
        
        // Hide the error as the user selects an option
        startLocationView.errorLabel.isHidden = true
        
        // Upon recieving a city selection we query the areas within the selected city
        let areasRef = Database.database().reference().child("city_areas/\(value)")
        
        // Create an empty String array that will hold the areas pulled from the database
        var areas = [String]()
        
            // Set the area dropdown button title to default
            startLocationView.setAreaButtonText(text: startLocationView.areaButtonDefaultTitle)
            
            // Remove the current values stored in the start areas array to make way for the areas of the selected city
            startAreas.removeAll()
            
            // Observe each city stored in the cities node
            areasRef.observeSingleEvent(of: .value, with: {(snapshot) in
                // Loop through the key:values and obtain the area values and downcast as string
                for child in snapshot.children {
                    let snap = child as! DataSnapshot
                    // We will present these values to the user to make a selection
                    let area = snap.value as! String
                    // Place the area into the array
                    areas.append(area)
                }
                // Set the start areas array equal to the, now populated, areas array
                self.startAreas = areas
            })
    }
}
