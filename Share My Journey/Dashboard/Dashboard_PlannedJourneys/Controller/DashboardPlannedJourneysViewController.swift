//
//  DashboardPlannedJourneysViewController.swift
//  Share My Journey
//
//  Created by Robert John Britton on 12/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit
import Firebase


class DashboardPlannedJourneysViewController: UITableViewController {

    private let cellId = "journeyCellId"
    private let headerId = "journeyHeaderId"
    private let footerId = "journeyFooterId"
    
    
    private var userJourneysRef: DatabaseReference!
    private var userJourneysHandle: DatabaseHandle!
    
    
    // Stored the Users' journeys and reloads the tableView to make the journey visible
    var userJourneys = [UserJourney]() {
        didSet {
            tableView.reloadData()
        }
    }
    
    override init(style: UITableViewStyle) {
        super.init(style: style)

    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nil, bundle: nil)
        
        tableView.separatorInset = UIEdgeInsets.zero
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
        
    override func viewDidLoad() {
        // register the header, cell and footer for the tableView section
        tableView.register(PlannedJourneyCell.self, forCellReuseIdentifier: cellId)
        tableView.register(PlannedJourneyHeaderView.self, forHeaderFooterViewReuseIdentifier: headerId)
        tableView.register(PlannedJourneyFooterView.self, forHeaderFooterViewReuseIdentifier: footerId)
        
        tableView.sectionHeaderHeight = 30
        tableView.sectionFooterHeight = 40
        
        observeDatabase()
    }
    
    
    private func observeDatabase() {
        // Get the initial journeys that the user owns when the view loads (this happens once)
        if let userId = Auth.auth().currentUser?.uid {

            self.userJourneysRef = Database.database().reference().child("user_journeys/\(userId)")

            // observes changes to the user journeys, pulling references from the node so we can find the
            // journey information in the journeys node
            self.userJourneysHandle = self.userJourneysRef.observe(.value, with: {(snapshot) in

                var journeys: [UserJourney] = [UserJourney]()

                for child in snapshot.children {
                    let snap = child as! DataSnapshot
                    let journeyKey = snap.key

                    let journeysRef = Database.database().reference().child("journeys/\(journeyKey)")

                    // Pulls journey information, which is identified by the reference from the query above
                    // Creates a user journey model which is added to a temp array. Once all journeys have been
                    // pulled, set the user journeys array in view controller, which reloads the collection view, displaying the journeys
                    journeysRef.observeSingleEvent(of: .value, with: {(journeySnapshot) in

                        // Create a dictionary to store the key:value JSon data from database
                        if let dictionary = journeySnapshot.value as? [String: AnyObject] {

                            let userJourney = UserJourney()
                            userJourney.journeyName = dictionary["journeyName"] as? String
                            userJourney.journeyId = dictionary["journeyId"] as? String
                            userJourney.startCity = dictionary["startCity"] as? String
                            userJourney.startArea = dictionary["startArea"] as? String
                            userJourney.startDate = dictionary["startDate"] as? Int
                            userJourney.endCity = dictionary["endCity"] as? String
                            userJourney.endArea = dictionary["endArea"] as? String
                            userJourney.endDate = dictionary["endDate"] as? Int
                            userJourney.driverId = dictionary["driverId"] as? String
                            userJourney.vehicleId = dictionary["vehicleId"] as? String
                            userJourney.maxPassengers = dictionary["maxPassengers"] as? Int
                            userJourney.pickupInfo = dictionary["pickupInfo"] as? String


                            // Append each new vehicle object the temporary array
                            journeys.append(userJourney)
                        }

                        // Setting the array here, instantly updates the collectionView with all the vehicles
                        self.userJourneys = journeys

                    })
                }
            })
        }
    }

    
    override func numberOfSections(in tableView: UITableView) -> Int {

        if userJourneys.count > 0 {
            tableView.separatorStyle = .singleLine
            tableView.backgroundView = nil
        } else {
            let noJourneysView = NoPlannedJourneyCell()
            tableView.backgroundView  = noJourneysView
            tableView.separatorStyle  = .none
        }
        return 1
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userJourneys.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! PlannedJourneyCell
        
        // Set the Format in which the dates will be displayed
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "E d MMM  @ h:mm"
        
        let startDate = FormatDate.formatInegerTimeInetervalToDate(timeInterval: userJourneys[indexPath.row].startDate!)
        let endDate = FormatDate.formatInegerTimeInetervalToDate(timeInterval: userJourneys[indexPath.row].endDate!)
        
        cell.journeyNameLabel.text = userJourneys[indexPath.row].journeyName
        cell.leavingDateLabel.text = dateFormatter.string(from: startDate)
        cell.arrivingDateLabel.text = dateFormatter.string(from: endDate)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerId)
        return header
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = tableView.dequeueReusableHeaderFooterView(withIdentifier: footerId) as! PlannedJourneyFooterView
        
        // Pass a reference of this view controller to the footer so that we can present the AddJourney view controller when the 'add journey' button is tapped.
        footer.tableViewController = self
        return footer
    }
    
}


