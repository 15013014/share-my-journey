//
//  NoPlannedJourneyCell.swift
//  Share My Journey
//
//  Created by Robert John Britton on 18/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit

class NoPlannedJourneyCell: UITableViewCell {
    
    let infoLabel: UILabel = {
        let label = UILabel()
        label.text =  """
        You currently have no journeys to display.\n
        To create a journey, tap the 'Add Journey' button\n
        The journeys you create will be displayed here.
        """
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.textAlignment = .center
        label.numberOfLines = 20
        label.textColor = UIColor(hex: Global.colour.darkGrey)
        return label
    }()
    
    override func didMoveToSuperview() {
        addSubview(infoLabel)
        
        infoLabel.snp.makeConstraints({(make) -> Void in
            make.centerX.centerY.equalToSuperview()
        })
    }
    
}
