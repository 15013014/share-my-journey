//
//  PlannedJourneyHeader.swift
//  Share My Journey
//
//  Created by Robert John Britton on 18/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit

class PlannedJourneyHeaderView:  UITableViewHeaderFooterView {
    
    let headerTitle: UILabel = {
        let label = UILabel()
        label.text = "Your Journeys"
        return label
    }()
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func didMoveToSuperview() {
        addSubview(headerTitle)
        headerTitle.snp.makeConstraints({(make) -> Void in
            make.top.right.bottom.equalToSuperview()
            make.left.equalToSuperview().offset(16)
        })
    }
    
    
}
