//
//  PlannedJourneyCell.swift
//  Share My Journey
//
//  Created by Robert John Britton on 18/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit

class PlannedJourneyCell: UITableViewCell {
    
    // May hold a reference to the tableViewController 
    var tableViewController: UITableViewController?
    
    // Holds the subviews
    let contentContainerView: UIView = {
        let view = UIView()
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor(hex: Global.colour.lightGrey).cgColor
        view.clipsToBounds = true
        view.layer.borderWidth = 1.0
        view.layer.cornerRadius = 8.0
        view.layer.borderColor = UIColor.clear.cgColor
        view.clipsToBounds = true
        return view
    }()
    
    let journeyNameLabel: UILabel = {
        let label = UILabel()
        label.text = "Loading"
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textAlignment = .center
        label.textColor = UIColor(hex: Global.colour.darkGrey)
        return label
    }()
    
    
    let leavingDateLabel: UILabel = {
        let label = UILabel()
        label.text = "Loading"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textAlignment = .center
        label.textColor = UIColor(hex: Global.colour.green)
        return label
    }()
    
    
    let arrivingDateLabel: UILabel = {
        let label = UILabel()
        label.text = "Loading"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textAlignment = .center
        label.textColor = UIColor(hex: Global.colour.lightRed)
        return label
    }()
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = UIColor.white
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToSuperview() {
        setupView()
    }
    
    private func setupView() {
        addSubview(contentContainerView)
        
        contentContainerView.addSubview(journeyNameLabel)
        contentContainerView.addSubview(leavingDateLabel)
        contentContainerView.addSubview(arrivingDateLabel)
        
        
        // Parent Container
        contentContainerView.snp.makeConstraints({(make) -> Void in
            make.top.left.equalToSuperview().offset(8)
            make.bottom.right.equalToSuperview().offset(-8)
        })
        
        
        // Info labels
        journeyNameLabel.snp.makeConstraints({(make) -> Void in
            make.top.left.right.equalToSuperview()
            make.height.equalToSuperview().dividedBy(2)
        })
        
        leavingDateLabel.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(journeyNameLabel.snp.bottom)
            make.left.bottom.equalToSuperview()
            make.width.equalToSuperview().dividedBy(2)
        })
        
        arrivingDateLabel.snp.makeConstraints({(make) -> Void in
            make.top.bottom.equalTo(leavingDateLabel)
            make.left.equalTo(leavingDateLabel.snp.right)
            make.right.equalToSuperview()
        })
        
    }
}
