//
//  PlannedJourneyFooterView.swift
//  Share My Journey
//
//  Created by Robert John Britton on 18/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit

class PlannedJourneyFooterView: UITableViewHeaderFooterView {

    var tableViewController: DashboardPlannedJourneysViewController?
    
    let addJourneyButton: UIButton = {
        let button = UIButton(type: UIButtonType.system)
        button.setTitle("Add Journey", for: .normal)
        button.addTarget(self, action: #selector(handleAddnewJourneyTapped), for: .touchUpInside)
        return button
    }()
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func didMoveToSuperview() {
        addSubview(addJourneyButton)
        addJourneyButton.snp.makeConstraints({(make) -> Void in
            make.edges.equalToSuperview()
        })
    }
    
    @objc func handleAddnewJourneyTapped() {
        let viewController = AddJourneyStartLocationViewController()
        
        tableViewController?.present(AddJourneyNavigationController(rootViewController: viewController), animated: true, completion: nil)
    }

}
