//
//  UserPrivateProfileViewController.swift
//  Share My Journey
//
//  Created by Robert John Britton on 29/03/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit
import Firebase


class UserPrivateProfileViewController: UIViewController {
    public var userProfile: UserProfile? {
        didSet {
            initialiseInputFieldText()
        }
    }
    
    var activeTextView: UITextView?
    
    let userPrivateProfileView: UserPrivateProfileView = {
        let view = UserPrivateProfileView()
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userPrivateProfileView.saveButtonDelegate = self
        userPrivateProfileView.textFieldDelegate = self
        userPrivateProfileView.textViewDelegate = self
        
        view.backgroundColor = .white
        
        setupNavigationBarItems()
        initialiseInputFieldText()
        setupView()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        registerKeyboardObservers()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        removeKeyboardObservers()
    }
    
    private func setupNavigationBarItems() {
        // Include the users' username in the navigation title if its available
        navigationItem.title = "Your Profile Details"
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(handleDoneButtonTouch))
    }
    
    
    // Check that the recieved userProfile model object is not nil, then populate the textfields in the view with the model data
    private func initialiseInputFieldText() {
        if let profile = userProfile {
            let profileImage = profile.profileImageURL
            userPrivateProfileView.userProfileImageView.image = UIImage(named: profileImage)
            
            userPrivateProfileView.inlineUsername.textField.text = profile.username
            userPrivateProfileView.inlineFirstName.textField.text = profile.firstName
            userPrivateProfileView.inlineLastName.textField.text = profile.lastName
            userPrivateProfileView.userBioTextView.text = profile.bio
            userPrivateProfileView.inlineUserGender.textField.text = profile.gender
            
            // Convert the dob time interval into a date. Get todays date then compare the two to find users age.
            let dateOfBirth: Date = FormatDate.formatInegerTimeInetervalToDate(timeInterval: profile.dateOfBirth) // Date(timeIntervalSince1970: TimeInterval(profile.dateOfBirth))
            let date = Date()
            let years = date.offset(date: dateOfBirth)
            
            userPrivateProfileView.inlineUserAge.textField.text = years
        }
    }
    
    private func setupView() {
        view.addSubview(userPrivateProfileView)
        userPrivateProfileView.snp.makeConstraints({(make) -> Void in
            make.edges.equalToSuperview()
        })
    }
        
    
    @objc private func handleDoneButtonTouch() {
        dismiss(animated: true, completion: nil)
    }
}


// Class extension that handles the views' textfield interactions
extension UserPrivateProfileViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
}


// Class extension that handles the views' textView interactions
extension UserPrivateProfileViewController: UITextViewDelegate {
    // Resign the keyboard as the text view begins to end editing
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        textView.resignFirstResponder()
        return true
    }
    
    // Resign the keyboard if the user touches the return key. This method is in place due to the default action of the return key for a textView being to start a new line.
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
    
    // When the textView is not editiin, or editing has finished, the active text view is set to nil.
    func textViewDidEndEditing(_ textView: UITextView) {
        textView.resignFirstResponder()
    }
    
}


extension UserPrivateProfileViewController: UserPrivateProfileViewDelegate {
    /// The method that is triggered when the user taps the save button
    func saveButtonTapped() {
        
        // Three items to check for empty: username; first name; last name
        guard !userPrivateProfileView.inlineUsername.textField.text!.isEmpty else { return }
        guard !userPrivateProfileView.inlineFirstName.textField.text!.isEmpty else { return }
        guard !userPrivateProfileView.inlineLastName.textField.text!.isEmpty else { return }
        
        // after passing conditions edit the user profile information on the database
        editUserProfileDetails()
    }
    
    private func editUserProfileDetails() {
        
        // Get the users id from the profile model
        // Get the user id of the currently logged in user
        // Check that the user id's match before making changes to the users profile in databse
        
        guard let userId = userProfile?.userId else { return }
        guard let uid = Auth.auth().currentUser?.uid else { return }
        guard userId == uid else { return }
        
        // Create a reference to the users nodes in the users table
        let ref = Database.database().reference().child("users/\(userId)")
        
        let newUsername = ["username":"\(userPrivateProfileView.inlineUsername.textField.text!)"]
        let newFirstName = ["firstName":"\(userPrivateProfileView.inlineFirstName.textField.text!)"]
        let newLastName = ["lastName":"\(userPrivateProfileView.inlineLastName.textField.text!)"]
        let newBio = ["bio":"\(userPrivateProfileView.userBioTextView.text!)"]
        
        
        ref.updateChildValues(newUsername) { (nil, ref) in
            ref.updateChildValues(newFirstName)
            ref.updateChildValues(newLastName)
            ref.updateChildValues(newBio)

            self.presentAlert(title: "Update Success", message: "Your details have been updated successfully.")
        }
        
    }
    
    // Possible change could be a check to see if the text in each textfield is equal to what is stored in the user profile model
    // and only update the fields that are different. However this methos does as intended for now.
    
}




// Class extension that handles the scrollView position when the on-screen keyboard is visible
extension UserPrivateProfileViewController {
    func registerKeyboardObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardDidHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardDidChangeFrame, object: nil)
    }
    
    func removeKeyboardObservers() {
        NotificationCenter.default.removeObserver(self, name:NSNotification.Name.UIKeyboardDidHide, object: nil)
        NotificationCenter.default.removeObserver(self, name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.removeObserver(self, name:NSNotification.Name.UIKeyboardDidChangeFrame, object: nil)
    }
    
    @objc func keyboardWillShow(notification:NSNotification){
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = userPrivateProfileView.scrollView.contentInset
        
        // Create an inset at the bottom of the scrollView which is equal to the height of the keyboard
        contentInset.bottom = keyboardFrame.size.height
        
        // Assign updated inset to the scrollView
        userPrivateProfileView.scrollView.contentInset = contentInset
        
        // This condition check if the first responder is the textView. If it is the scrollview scrolls down to its location
        if userPrivateProfileView.userBioTextView.isFirstResponder {
            userPrivateProfileView.scrollView.scrollRectToVisible(userPrivateProfileView.userBioTextView.frame, animated: true)
        }
    }
    
    @objc func keyboardWillHide(notification:NSNotification){
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        userPrivateProfileView.scrollView.contentInset = contentInset
    }
}

