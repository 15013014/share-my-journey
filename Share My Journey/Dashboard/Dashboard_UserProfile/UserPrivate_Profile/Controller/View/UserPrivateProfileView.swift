//
//  UserPrivateProfileView.swift
//  Share My Journey
//
//  Created by Robert John Britton on 29/03/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit


protocol UserPrivateProfileViewDelegate {
    func saveButtonTapped()
}

class UserPrivateProfileView: UIView {
        
    var textFieldDelegate: UITextFieldDelegate? {
        didSet { registerTextFieldDelegates() }
    }
    
    var textViewDelegate: UITextViewDelegate? {
        didSet { resgisterTextViewDelegate() }
    }
    
    var saveButtonDelegate: UserPrivateProfileViewDelegate?
    
    //-------------Define the UIComponents to add to the view
    
    let userProfileImageView: UIImageView = {
        let img = UIImage()
        let iv = UIImageView(image: img)
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    
    // -- USERNAME
    let inlineUsername: InlineTextFieldView = {
        let view = InlineTextFieldView()
        view.label.textColor = UIColor(hex: Global.colour.darkGrey)
        view.label.text = Global.titleText.label.username
        view.textField = PromptTextField()
        return view
    }()
    
    
    // -- FIRSTNAME
    let inlineFirstName: InlineTextFieldView = {
        let view = InlineTextFieldView()
        view.label.textColor = UIColor(hex: Global.colour.darkGrey)
        view.label.text = Global.titleText.label.firstname
        view.textField = PromptTextField()
        return view
    }()
    

    // -- LASTNAME
    let inlineLastName: InlineTextFieldView = {
        let view = InlineTextFieldView()
        view.label.textColor = UIColor(hex: Global.colour.darkGrey)
        view.label.text = Global.titleText.label.lastname
        view.textField = PromptTextField()
        return view
    }()
    
    
    // -- AGE
    let inlineUserAge: InlineTextFieldView = {
        let view = InlineTextFieldView()
        view.label.textColor = UIColor(hex: Global.colour.darkGrey)
        view.label.text = Global.titleText.label.age
        view.textField = ReadOnlyTextField()
        return view
    }()
    
    
    // -- GENDER
    let inlineUserGender: InlineTextFieldView = {
        let view = InlineTextFieldView()
        view.label.textColor = UIColor(hex: Global.colour.darkGrey)
        view.label.text = Global.titleText.label.gender
        view.textField = ReadOnlyTextField()
        return view
    }()
    

    // -- BIO
    let userBioContainer: UIView = {
        return UIView()
    }()
    
    let userBioTextViewLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hex: Global.colour.darkGrey)
        label.text = Global.titleText.label.bio
        return label
    }()
    
    let userBioTextView: UITextView = {
        let textView = UITextView()
        textView.backgroundColor = UIColor(hex: Global.colour.white)
        textView.layer.cornerRadius = 10
        textView.layer.borderWidth = 1
        textView.layer.borderColor = UIColor(hex: Global.colour.lightGrey).cgColor
        textView.clipsToBounds = true
        textView.font = UIFont.systemFont(ofSize: 14)
        let contentInstets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        textView.contentInset = contentInstets
        textView.returnKeyType = .done
        return textView
    }()
    
    
    // -- SAVE BUTTON
    let saveChangesButton: UIButton = {
        let button = UIButton(type: UIButtonType.system).defaultButton(title: Global.titleText.button.saveChanges)
        button.addTarget(self, action: #selector(handleSaveChangesButtonTouch), for: .touchUpInside)
        return button
    }()
    
    // Outer scroallable content wrapper
    let scrollView: UIScrollView = {
        let view = UIScrollView()
        view.isScrollEnabled = true
        return view
    }()
    
    // Inner content wrapper
    let contentView: UIView = {
        let view = UIView()
        return view
    }()
    
    
    override func didMoveToSuperview() {
        addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(userProfileImageView)
        contentView.addSubview(inlineUsername)
        contentView.addSubview(inlineUserGender)
        contentView.addSubview(inlineFirstName)
        contentView.addSubview(inlineLastName)
        contentView.addSubview(inlineUserAge)
        contentView.addSubview(userBioTextViewLabel)
        contentView.addSubview(userBioTextView)
        contentView.addSubview(saveChangesButton)
        setupViews()
        
    }
    
    private func setupViews() {
        scrollView.snp.makeConstraints({(make) -> Void in
            make.top.left.right.bottom.equalTo(0)
        })
        
        contentView.snp.makeConstraints({(make) -> Void in
            make.top.left.right.bottom.equalTo(0)
            make.width.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(1).priority(250)
        })
        
        userProfileImageView.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(contentView.safeAreaLayoutGuide).offset(16)
            make.centerX.equalToSuperview()
            make.height.width.equalTo(100)
        })
        
        inlineUsername.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(userProfileImageView.snp.bottom).offset(16)
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.height.equalTo(50)
        })
        
        inlineUserGender.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(inlineUsername.snp.bottom).offset(16)
            make.left.right.height.equalTo(inlineUsername)
        })
        
        inlineFirstName.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(inlineUserGender.snp.bottom).offset(16)
            make.left.right.height.equalTo(inlineUsername)
        })
        
        inlineLastName.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(inlineFirstName.snp.bottom).offset(16)
            make.left.right.height.equalTo(inlineUsername)
        })
        
        inlineUserAge.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(inlineLastName.snp.bottom).offset(16)
            make.left.right.height.equalTo(inlineUsername)
        })
        
        userBioTextViewLabel.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(inlineUserAge.snp.bottom).offset(16)
            make.left.right.height.equalTo(inlineUsername)
        })
        
        userBioTextView.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(userBioTextViewLabel.snp.bottom)
            make.left.right.equalTo(inlineUsername)
            make.height.equalTo(100)
        })
        
        saveChangesButton.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(userBioTextView.snp.bottom).offset(16)
            make.left.right.height.equalTo(inlineUsername)
            make.bottom.equalTo(contentView.snp.bottom)
        })
    }
  
    private func registerTextFieldDelegates() {
        inlineUsername.textField.delegate = textFieldDelegate
        inlineFirstName.textField.delegate = textFieldDelegate
        inlineLastName.textField.delegate = textFieldDelegate
    }
    
    private func resgisterTextViewDelegate() {
        userBioTextView.delegate = textViewDelegate
    }


    @objc private func handleSaveChangesButtonTouch() {
        self.saveButtonDelegate?.saveButtonTapped()
    }
}

