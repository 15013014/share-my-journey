//
//  VehicleMakeViewController.swift
//  Share My Journey
//
//  Created by Robert John Britton on 15/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit

class AddVehicleMakeViewController: UIViewController, AddVehicleViewDelegate, UITextFieldDelegate {
    
    /// The user model that is updated within this view controller then passed to the next view controller.
    /// Contains the require user vehicle information that will be get added to the database.
    var userVehicle = UserVehicle()
    
    /// The view to display is customised here
    private lazy var makeView: AddVehicleView = {
        let view = AddVehicleView()
        view.delegate = self
        view.textField.delegate = self
        view.titleLabel.text = "What Make is Your Vehicle?"
        view.errorLabel.text = "Error: You must specifiy the vehicle's make"
        return view
    }()
    
    /// Customise the view controllers container view as soon as it has loaded.
    override func viewDidLoad() {
        navigationItem.title = "Your Vehicle Details"
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(navigationBackButtonTapped))
        
        view.backgroundColor = UIColor.white
        
        setupView()
        makeView.textField.becomeFirstResponder()
    }
    
    /// Adds the view to the container view stack and specify auto layout constraints.
    private func setupView() {
        view.addSubview(makeView)
        makeView.snp.makeConstraints({(make) -> Void in
            make.edges.equalToSuperview()
        })
    }
    
    /// This method is called when the user taps the back button in the navbar and tells the navigaation controller to
    /// pop this view controller from the navigation stack. This reverts back to the previous view controller.
    @objc private func navigationBackButtonTapped() {
        view.endEditing(true)
        navigationController?.popViewController(animated: true)
    }
    
    /// The continue button action method that is called when the user taps the continue button.
    /// It first checks if the texfield has text before telling the navigation controller push to the next view controller.
    /// If the textfield is empty, display the error and return out.
    func handleContinueButtonTouch() {
        if !makeView.textField.text!.isEmpty {
            
            userVehicle.make = makeView.textField.text!
            
            let nextViewController = AddVehicleModelViewController()
            nextViewController.userVehicle = userVehicle
            navigationController?.pushViewController(nextViewController, animated: true)
            return
        }
        makeView.errorLabel.isHidden = false
    }

    
    /// Always hides the error as the user begins entering text into texfield.
    func textFieldDidBeginEditing(_ textField: UITextField) {
        makeView.errorLabel.isHidden = true
    }
    
    
    /// Either hides or shows the error based on the result of checking if the textField has text after the user is done editing
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        makeView.errorLabel.isHidden = !(makeView.textField.text!.isEmpty)
        return true
    }
    
    
    /// This method is called after the user taps the return key.
    /// The keyboard is hidden and the continue button method is called, which, for convenience will transition to the next screen
    /// if conditions are met.
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        handleContinueButtonTouch()
        return true
    }
    
}
