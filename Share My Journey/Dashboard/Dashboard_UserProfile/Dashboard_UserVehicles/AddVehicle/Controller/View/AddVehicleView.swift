//
//  AddVehicleView.swift
//  Share My Journey
//
//  Created by Robert John Britton on 09/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit



protocol AddVehicleViewDelegate {
    func handleContinueButtonTouch()
}

class AddVehicleView: UIView {
    
    var delegate: AddVehicleViewDelegate?
    
    var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hex: Global.colour.darkGrey)
        label.font = UIFont.systemFont(ofSize: 18, weight: UIFont.Weight.bold)
        label.numberOfLines = 1
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    var subTitleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.light)
        label.numberOfLines = 4
        label.lineBreakMode = .byTruncatingTail
        label.adjustsFontSizeToFitWidth = false
        return label
    }()
    
    var textField: BaseTextField = {
        let textField: RoundedTextField = RoundedTextField()
        textField.characterLimit = 20
        textField.validCharacters = Global.validCharSet.textNumericOnly
        textField.setLeftContentInset(nil)
        textField.clearButtonMode = .never
        textField.textAlignment = .center
        textField.autocapitalizationType = .allCharacters
        return textField
    }()
    
    var errorLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hex: Global.colour.red)
        label.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.bold)
        label.numberOfLines = 4
        label.lineBreakMode = .byTruncatingTail
        label.textAlignment = .center
        label.isHidden = true
        label.adjustsFontSizeToFitWidth = false
        return label
    }()
    
    var continueButton: UIButton = {
        let button = UIButton() .defaultButton(title: Global.titleText.button.Continue)
        button.addTarget(self, action: #selector(handleContinueButtonTouch), for: .touchUpInside)
        return button
    }()
    
    override func didMoveToSuperview() {
        setupView()
    }
    
    func setupView() {
        addSubview(titleLabel)
        addSubview(subTitleLabel)
        addSubview(textField)
        addSubview(errorLabel)
        addSubview(continueButton)
        
        
        titleLabel.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(safeAreaLayoutGuide).offset(64)
            make.centerX.equalToSuperview()
            make.left.equalTo(safeAreaLayoutGuide).offset(16)
        })
        
        subTitleLabel.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(titleLabel.snp.bottom).offset(8)
            make.centerX.equalToSuperview()
            make.left.equalToSuperview().offset(16)
            make.right.equalTo(titleLabel.snp.right)
        })
        
        
        textField.snp.makeConstraints({(make) -> Void in
            make.centerX.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.height.equalTo(50)
        })
        
        
        errorLabel.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(textField.snp.bottom).offset(4)
            make.centerX.equalToSuperview()
            make.width.equalTo(textField.snp.width)
        })
        
        continueButton.snp.makeConstraints({(make) -> Void in
            make.centerX.equalToSuperview()
            make.width.equalTo(textField.snp.width)
            make.height.equalTo(40)
            make.bottom.equalTo(safeAreaLayoutGuide).offset(-16)
        })
    }
    
    @objc func handleContinueButtonTouch() {
        self.delegate?.handleContinueButtonTouch()
    }
}

