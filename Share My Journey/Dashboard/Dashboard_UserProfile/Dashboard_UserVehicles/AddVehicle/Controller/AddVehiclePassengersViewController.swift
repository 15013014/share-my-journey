//
//  VehiclePassengersViewController.swift
//  Share My Journey
//
//  Created by Robert John Britton on 15/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit
import Firebase


class AddVehiclePassengersViewController: UIViewController, AddVehicleViewDelegate, UITextFieldDelegate {

    var userVehicle = UserVehicle()
    
    lazy var textField: BaseTextField = {
        let textField: RoundedReadOnlyTextField = RoundedReadOnlyTextField()
        textField.validCharacters = Global.validCharSet.numericOnly
        textField.text = String(Int(stepper.value))
        textField.characterLimit = 1
        textField.setLeftContentInset(nil)
        textField.clearButtonMode = .never
        textField.textAlignment = .center
        return textField
    }()
    
    var stepper: UIStepper = {
        let stepper = UIStepper()
        stepper.value = 1
        stepper.minimumValue = 1
        stepper.maximumValue = 4
        stepper.stepValue = 1
        stepper.tintColor = UIColor(hex: Global.colour.darkGrey)
        stepper.addTarget(self, action: #selector(handleNoOfSeatsStepperTouch), for: .touchUpInside)
        return stepper
    }()
    
    private lazy var passengerView: AddVehicleView = {
        let view = AddVehicleView()
        view.textField = self.textField
        view.delegate = self
        view.textField.delegate = self
        view.titleLabel.text = "How Many Passenger Seats?"
        view.subTitleLabel.text = "You may wish to leave the middle seat free for passenger comfort."
        return view
    }()
    
    
    override func viewDidLoad() {
        navigationItem.title = "Your Vehicle Details"
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(navigationBackButtonTapped))
        
        view.backgroundColor = UIColor.white
        
        setupView()
    }
    
    private func setupView() {
        view.addSubview(passengerView)
        passengerView.addSubview(stepper)
        
        passengerView.snp.makeConstraints({(make) -> Void in
            make.edges.equalToSuperview()
        })
        
        stepper.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(textField.snp.bottom).offset(8)
            make.centerX.equalToSuperview()
        })
    }
    
    @objc private func navigationBackButtonTapped() {
        navigationController?.popViewController(animated: true)
    }
    
    
    func handleContinueButtonTouch() {
        if !passengerView.textField.text!.isEmpty {
            
            userVehicle.passengerSeats = Int(passengerView.textField.text!)!
            
            enterUserVehicleIntoDatabase()
        }
    }
    
    @objc private func handleNoOfSeatsStepperTouch() {
        textField.text = String(Int(stepper.value))
    }
    
    /// Enters the vehicle information from the UserVehicle model into the databse then dismisses the viewController hierarchy
    private func enterUserVehicleIntoDatabase() {
        
        //Get the uid from the currently signed in user
        if let userId = Auth.auth().currentUser?.uid {
            // add this uid to the vehicle model prior to upload (helps identify which user the vehicle belongs)
            userVehicle.userId = userId
            
            // Create a reference to the root of database
            let ref = Database.database().reference()
            
            let vehiclesRef = ref.child("vehicles")
            
            // Go to the users node within the user vehicles node
            let userVehiclesRef = ref.child("user_vehicles")
            
            // Create and store a child node within user vehicles
            let vehicleKey = userVehiclesRef.childByAutoId().key
            
            // Create map object with key:Value pairs
            // store the vehicle owners id along with the vehicle id so we can query this vehicle later
            let post: Any = [
                "make": "\(userVehicle.make)",
                "model": "\(userVehicle.model)",
                "colour": "\(userVehicle.colour)",
                "passengerSeats": userVehicle.passengerSeats,
                "registration": "\(userVehicle.registration)",
                "vehicleId": "\(vehicleKey)",
                "userId":"\(userId)"
            ];
            
            // Create a new vehicle in the vehicles node
            vehiclesRef.child("\(vehicleKey)").setValue(post)
            
            // Place a reference between the user and the owned vehicles in the user vehicles node
            userVehiclesRef.child("\(userId)/\(vehicleKey)").setValue(true)
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
