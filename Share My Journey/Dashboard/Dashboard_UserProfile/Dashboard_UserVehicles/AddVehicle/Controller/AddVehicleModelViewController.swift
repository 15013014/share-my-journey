//
//  VehicleModelViewController.swift
//  Share My Journey
//
//  Created by Robert John Britton on 15/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit

class AddVehicleModelViewController: UIViewController, AddVehicleViewDelegate, UITextFieldDelegate {
    
    var userVehicle = UserVehicle()
    
    private lazy var modelView: AddVehicleView = {
        let view = AddVehicleView()
        view.delegate = self
        view.textField.delegate = self
        view.titleLabel.text = "What Model is Your Vehicle?"
        view.errorLabel.text = "Error: You must specifiy the vehicle's model"
        return view
    }()
    
    override func viewDidLoad() {

        navigationItem.title = "Your Vehicle Details"
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(navigationBackButtonTapped))
        
        view.backgroundColor = UIColor.white
        
        modelView.textField.becomeFirstResponder()
        
        setupView()
    }
    
    private func setupView() {
        view.addSubview(modelView)
        
        modelView.snp.makeConstraints({(make) -> Void in
            make.edges.equalToSuperview()
        })
    }
    
    @objc private func navigationBackButtonTapped() {
        view.endEditing(true)
        navigationController?.popViewController(animated: true)
    }
    
    
    func handleContinueButtonTouch() {
        if !modelView.textField.text!.isEmpty {
            
            userVehicle.model = modelView.textField.text!
            
            let nextViewController = AddVehicleColourViewController()
            nextViewController.userVehicle = userVehicle
            navigationController?.pushViewController(nextViewController, animated: true)
            return
        }
        modelView.errorLabel.isHidden = false
    }
    

    // TexftField delegate methods
    func textFieldDidBeginEditing(_ textField: UITextField) {
        modelView.errorLabel.isHidden = true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        // If the textfield is empty, show the error msg, other hide the error message
        modelView.errorLabel.isHidden = !(modelView.textField.text!.isEmpty)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        handleContinueButtonTouch()
        return true
    }

}
