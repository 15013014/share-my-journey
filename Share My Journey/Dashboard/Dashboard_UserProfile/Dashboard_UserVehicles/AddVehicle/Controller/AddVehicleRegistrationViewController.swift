//
//  VehicleRegistrationViewController.swift
//  Share My Journey
//
//  Created by Robert John Britton on 15/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit

class AddVehicleRegistrationViewController: UIViewController, AddVehicleViewDelegate, UITextFieldDelegate {

    let userVehicle = UserVehicle()
    
    private lazy var resgistrationView: AddVehicleView = {
        let view = AddVehicleView()
        view.delegate = self
        view.textField.delegate = self
        view.textField.characterLimit = 8
        view.subTitleLabel.text = "We will only share this with people who's journey matches yours."
        view.titleLabel.text = "What is Your Vehicle's Registration?"
        view.errorLabel.text = "Error: You must specify your vehicle's registration"
        return view
    }()
    
    override func viewDidLoad() {
        
        navigationItem.title = "Your Vehicle Details"
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(navigationBackButtonTapped))
        
        view.backgroundColor = UIColor.white

        setupView()
    }
    
    private func setupView() {
        view.addSubview(resgistrationView)
        
        resgistrationView.snp.makeConstraints({(make) -> Void in
            make.edges.equalToSuperview()
        })
    }
    
    // As this is the rootViewController, we will dismiss the viewController if the user taps the back button
    @objc private func navigationBackButtonTapped() {
        view.endEditing(true)
        dismiss(animated: true, completion: nil)
    }
    
    
    func handleContinueButtonTouch() {
        if !resgistrationView.textField.text!.isEmpty {
            
            userVehicle.registration = resgistrationView.textField.text!
            
            let nextViewController = AddVehicleMakeViewController()
            nextViewController.userVehicle = userVehicle
            navigationController?.pushViewController(nextViewController, animated: true)
            return
        }
        resgistrationView.errorLabel.isHidden = false
    }
    
    
    // TexftField delegate methods
    func textFieldDidBeginEditing(_ textField: UITextField) {
        resgistrationView.errorLabel.isHidden = true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        // If the textfield is empty, show the error msg, other hide the error message
        resgistrationView.errorLabel.isHidden = !(resgistrationView.textField.text!.isEmpty)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        handleContinueButtonTouch()
        return true
    }
}
