//
//  VehicleColourViewController.swift
//  Share My Journey
//
//  Created by Robert John Britton on 15/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit

class AddVehicleColourViewController: UIViewController, AddVehicleViewDelegate, UITextFieldDelegate {

    var userVehicle = UserVehicle()
    
    private lazy var colourView: AddVehicleView = {
        let view = AddVehicleView()
        view.delegate = self
        view.textField.delegate = self
        view.textField.validCharacters = Global.validCharSet.textOnly
        view.titleLabel.text = "What Colour is Your Vehicle?"
        view.errorLabel.text = "Error: You must specifiy the vehicle's colour"
        return view
    }()
    
    override func viewDidLoad() {
        view.backgroundColor = UIColor.white
        
        navigationItem.title = "Your Vehicle Details"
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(navigationBackButtonTapped))
        
        colourView.textField.becomeFirstResponder()
        
        setupView()
    }
    
    
    private func setupView() {
        view.addSubview(colourView)
        
        colourView.snp.makeConstraints({(make) -> Void in
            make.edges.equalToSuperview()
        })
        
    }
    
    @objc private func navigationBackButtonTapped() {
        view.endEditing(true)
        navigationController?.popViewController(animated: true)
    }
    
    func handleContinueButtonTouch() {
        if !colourView.textField.text!.isEmpty {
            
            userVehicle.colour = colourView.textField.text!
            
            let nextViewController = AddVehiclePassengersViewController()
            nextViewController.userVehicle = userVehicle
            navigationController?.pushViewController(nextViewController, animated: true)
            return
        }
        colourView.errorLabel.isHidden = false
    }
    
    // TexftField delegate methods
    func textFieldDidBeginEditing(_ textField: UITextField) {
        colourView.errorLabel.isHidden = true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        // If the textfield is empty, show the error msg, other hide the error message
        colourView.errorLabel.isHidden = !(colourView.textField.text!.isEmpty)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        handleContinueButtonTouch()
        return true
    }

}
