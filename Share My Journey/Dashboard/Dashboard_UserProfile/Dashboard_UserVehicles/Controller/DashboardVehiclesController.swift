//
//  DashboardVehiclesController.swift
//  Share My Journey
//
//  Created by Robert John Britton on 25/03/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit
import Firebase


class DashboardVehiclesController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    // The string the collection view needs to identify a vehicle cell object
    private let cellId: String = "vehicle"
    
    private let maxNoOfVehiclesAllowed: Int = 3
    
    // Store the user vehicles node reference once we obtain the users id
    // this variable is object is used along with the user VehicleHandle when we
    // want to stop observnig the database (e.g. when the view has disappeared)
    private var userVehiclesRef: DatabaseReference!
    
    // Variable used to store reference to the observer, which is removed when view is no longer needed
    private var userVehiclesHandle: DatabaseHandle!
    
    // A button the user can tap to add a new vehicle, depending on if the max number of allowed vehicles has been reached.
    private lazy var addVehicleButton: UIButton = {
        let button = UIButton(type: UIButtonType.system)
        button.setTitle("Add Vehicle", for: .normal)
        button.layer.cornerRadius = 0
        button.backgroundColor = UIColor(hex: Global.colour.lightLightGrey)
        return button
    }()
    
    // A simple view that is displayed when the user has no vehicles to display. This view informs the user
    // on how to add a vehicle.
    private let noVehiclesInfoView: NoVehiclesInfoView = {
        let view = NoVehiclesInfoView()
        return view
    }()
    
    // This array stores the vehicle model objects which are populated by the database.
    // Whenever this array is set, the collectionView is reloaded, updating the cells displayed to the user
    // A method is then called to check the number of vehicle objects in this array.
    // Once check the method determines whether or not to display a button to allow the user to add a vehicle
    // depending on if the allowed number of vehicles has been reached. If no vehicles exist, the NoVehiclesInformationView
    // is displayed.
    private var vehicle = [UserVehicle]() {
        didSet {
            collectionView?.reloadData()
            enableAddVehicleButton()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        enableAddVehicleButton()
        
        collectionView?.showsVerticalScrollIndicator = false
        
        collectionView?.dataSource = self
        collectionView?.delegate = self
        
        collectionView?.backgroundColor = UIColor(hex: Global.colour.white)
        collectionView?.register(DashboardVehicleCell.self, forCellWithReuseIdentifier: cellId)
        collectionView?.contentInset.top = 8
        
        // Add observer when view loads, adding when view appears causes the Add vehicle button to flash up each time the
        // view appears. Adding the method here means that we can't remove the observer, as this method will only be called
        // once.
        observeDatabase()
    }

//    override func viewWillAppear(_ animated: Bool) {
//        // Sets up a listner that triggers any time a vehicle is added, edited or removed
//        observeDatabase()
//    }

//    // Removes the database observer when the view disappears
//    override func viewDidDisappear(_ animated: Bool) {
//        userVehiclesRef.removeObserver(withHandle: userVehiclesHandle)
//    }
    
    /// Creates an observer that triggers whenver a specified node has it value changed. Initially pulls the vehicles from the database.
    /// This method is run once, when the view loads for now...
    private func observeDatabase() {
        // Get the initial vehicles that the user owns when the view loads (this happens once)
        if let userId = Auth.auth().currentUser?.uid {
            
            self.userVehiclesRef = Database.database().reference().child("user_vehicles/\(userId)")
            
            // observes changes to the user vehicles, pulling references from the node som we can find the
            // vehicle information in the vehicles node
            self.userVehiclesHandle = self.userVehiclesRef.observe(.value, with: {(snapshot) in

                var vehicles: [UserVehicle] = [UserVehicle]()

                for child in snapshot.children {
                    let snap = child as! DataSnapshot
                    let vehicleKey = snap.key
                
                    let vehiclesRef = Database.database().reference().child("vehicles/\(vehicleKey)")
                    
                    // Pulls vehicle information, whic is identified by the reference from the query above
                    // Creates a user vehicle model which is added to a temp array. Once all vehicles have been
                    // pulled, set the vehicle array in view controller, which reloads the collection view
                    vehiclesRef.observeSingleEvent(of: .value, with: {(vehicleSnapshot) in
                        
                        // Create a dictionary to store the key:value JSon data from database
                        if let dictionary = vehicleSnapshot.value as? [String: AnyObject] {
                            
                            let userVehicle = UserVehicle()
                            userVehicle.colour = dictionary["colour"] as! String
                            userVehicle.make = dictionary["make"] as! String
                            userVehicle.model = dictionary["model"] as! String
                            userVehicle.passengerSeats = dictionary["passengerSeats"] as! Int
                            userVehicle.registration = dictionary["registration"] as! String
                            userVehicle.userId = dictionary["userId"] as! String
                            userVehicle.vehicleId = dictionary["vehicleId"] as! String
                            
                            // Append each new vehicle object the temporary array
                            vehicles.append(userVehicle)
                        }
                        
                        // Setting the array here, instantly updates the collectionView with all the vehicles
                        self.vehicle = vehicles
                    })
                }
            })
        }
    }
    
    /// Determines whether to add or remove the add vehicles button based on the number of userVehicle objects currently in the array
    /// The user is only allowed up to 3 vehicles registered to their account
    private func enableAddVehicleButton() {
        if self.vehicle.count < maxNoOfVehiclesAllowed || self.vehicle.isEmpty{
            self.displayAddVehicleButton()
        } else {
            self.removeAddVehicleButton()
        }
        
        enableInfoView()
    }
    
    /// Adds the 'Add Vehicle' button to the view, positions it and adds an inset to the collectionview to accomodate the height of the button
    /// Also adds a touch event listner which calls a method when the button is tapped
    private func displayAddVehicleButton() {
        view.addSubview(addVehicleButton)
        
        addVehicleButton.snp.makeConstraints({(make) -> Void in
            make.bottom.equalToSuperview()
            make.height.equalTo(40)
            make.left.right.equalToSuperview()
        })
        
        addVehicleButton.addTarget(self, action: #selector(addVehicleButtonTouch), for: .touchUpInside)
        collectionView?.contentInset.bottom = addVehicleButton.frame.height
    }
    
    /// Removes the 'Add Vehicle' button from the view, removes its associated layout constraints then resets the collection view insets
    /// Removes the touch event listner from the button for app efficiency
    private func removeAddVehicleButton() {
        addVehicleButton.removeTarget(self, action: #selector(addVehicleButtonTouch), for: .touchUpInside)
        addVehicleButton.snp.removeConstraints()
        addVehicleButton.removeFromSuperview()
        
        // remove the bottom inset as the button is no longer visible
        collectionView?.contentInset.bottom = 0
    }

    /// Determines whether to add or remove the 'NoVehiclesInformationView' based on the number of vehicles currently on displayed
    /// The purpose for this view is to display instructions to the user and keep them informaed
    private func enableInfoView() {
        if self.vehicle.count == 0 {
            self.displayNoVehiclesInfoView()
        } else {
            self.removeNoVehiclesInfoPanel()
        }
    }
    
    /// Adds the 'NoVehiclesInormationView' to the container view and sets its layout constraints
    private func displayNoVehiclesInfoView() {
        view.addSubview(noVehiclesInfoView)
        noVehiclesInfoView.snp.makeConstraints({(make) -> Void in
            make.top.left.equalToSuperview().offset(32)
            make.right.equalToSuperview().offset(-32)
            make.bottom.equalToSuperview().offset(-64)
        })
    }
    
    /// Removes the 'NoVehiclesInormationView' from the container view after removing it's layout constraints
    private func removeNoVehiclesInfoPanel() {
        noVehiclesInfoView.snp.removeConstraints()
        noVehiclesInfoView.removeFromSuperview()
    }
}



extension DashboardVehiclesController {
    
    /// Presents the view so the user can add another vehicle
    @objc private func addVehicleButtonTouch() {
        
        present(NavigationControllerLight(rootViewController: AddVehicleRegistrationViewController()) , animated: true, completion: nil)
    
    }

}



extension DashboardVehiclesController {
    
    /// Return the number of items equal to the number of user vehicle model objects in the array
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return vehicle.count
    }
    
    /// Dequeus visible cells in the collection view, updating the contents of each cells as it pulled from the queue and onto the collection view stack
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // Pull a cell from the queue, set it up then return it for displaying
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! DashboardVehicleCell
        
        // Map the uservehicle to the cell using the current index path
        let userVehicle = vehicle[indexPath.item]
        cell.tag = indexPath.item
        
        cell.delegate = self // Ties the button presses to this viewController (i.e. button prsses call the method attatched to the inherited protocol)
        cell.vehicleImage = UIImage(named: "vehicle_image") // A default image is used for now
        cell.vehicleMakeModelLabelText = "\(userVehicle.make), \(userVehicle.model)"
        cell.vehicleColourLabelText = "\(userVehicle.colour)"
        cell.vehicleRegistrationLabelText = "\(userVehicle.registration)"
        return cell
    }
    
    /// Determines the size of the vehicle cell within the collection view
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width-32, height: 180)
    }
    
}



extension DashboardVehiclesController: DashboardVehicleCellDelegate {
    
    /// This method is invoked when a 'Remove' button is touched. The buttons parent view tag is recieved.
    /// This tag, set when the collection view created the view, is used to retrive the vehicle from the array.
    func viewVehicleButtonTouched(viewTag: Int) {
        print(viewTag)
    }
    
    /// This method is invoked when a 'Remove' button is touched. The buttons parent view tag is recieved.
    /// This tag, set when the collection view created the view, is used to retrive the vehicle from the array.
    func removeVehicleButtonTouched(viewTag: Int) {
        let tag = viewTag
        
        let userVehicle: UserVehicle = vehicle[tag]

        // Setup references to where we want to remove vehicle info
        let userVehiclesRef = Database.database().reference().child("user_vehicles/\(userVehicle.userId)").child("\(userVehicle.vehicleId)")
        let vehiclesRef = Database.database().reference().child("vehicles/\(userVehicle.vehicleId)")
        
        // Remove the vehicle info and reference from stored locations
        userVehiclesRef.removeValue()
        vehiclesRef.removeValue()
        
        // remove the vehicle from the vehicle model array
        vehicle.remove(at: tag)
    }
}
