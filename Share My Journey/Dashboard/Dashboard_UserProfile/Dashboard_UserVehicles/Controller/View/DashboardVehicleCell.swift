//
//  VehicleSnapshotCellCollectionViewCell.swift
//  Share My Journey
//
//  Created by Robert John Britton on 25/03/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit


protocol DashboardVehicleCellDelegate {
    func viewVehicleButtonTouched(viewTag: Int)
    func removeVehicleButtonTouched(viewTag: Int)
}


class DashboardVehicleCell: UICollectionViewCell {
    
    // This view will contain the vehicle image view along with the vehicle information labels
    private let contentContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(hex: Global.colour.darkGrey)
        return view
    }()
    
    // A container that will display a given UIImage.
    private let vehicleImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 8.0
        imageView.layer.borderWidth = 1.0
        imageView.layer.borderColor = UIColor.clear.cgColor
        imageView.clipsToBounds = true
        return imageView
    }()
    
    // A view that will contain the vehicle information labels, keeping them grouped for easier layout
    private let labelContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(hex: Global.colour.white)
        view.layer.cornerRadius = 8.0
        view.layer.borderColor = UIColor.clear.cgColor
        view.layer.borderWidth = 1.0
        return view
    }()
    
    // A label that will display the vehicles make and model
    private let vehicleMakeModelLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    // A label that will display the vehicles colour
    private let vehicleColourLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    // A label that will display the vehicles registration number
    private let vehicleRegistrationLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    // A view that seperates the content view from the buttons for asthetics
    private let contentLineSeperator: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(hex: Global.colour.lightGrey)
        return view
    }()
    
    // A button that, when the delegate is set, will have a target method attatched to it.
    // Whn the button is tapped, the target method is called, and the delegate invoked. The
    // Tag of this view is passed to the delegate method so the viewController can identify
    // the view which where the button was tapped.
    private let removeVehicleButton: UIButton = {
        let button = UIButton().warningButton(title: "Remove")
        return button
    }()
    
    // A button that, when the delegate is set, will have a target method attatched to it.
    // When the button is tapped, the target method is called, and the delegate invoked. The
    // tag of this view is passed to the delegate method so the viewController can identify
    // the view which where the button was tapped.
    private let viewVehicleButton: UIButton = {
        let button = UIButton().defaultButton(title: "View")
        return button
    }()
    
    // ----- Setters
    
    /// Sets the text that will be visible as the vehicles' make and model
    public var vehicleMakeModelLabelText: String = "" {
        didSet { self.vehicleMakeModelLabel.text = vehicleMakeModelLabelText }
    }
    
    /// Sets the text that will be visible as the vehicles' colour
    public var vehicleColourLabelText: String = "" {
        didSet { self.vehicleColourLabel.text = vehicleColourLabelText }
    }
    
    /// Sets the text that will be visible as the vehicles' registration
    public var vehicleRegistrationLabelText: String = "" {
        didSet { self.vehicleRegistrationLabel.text = vehicleRegistrationLabelText }
    }
    
    public var vehicleImage: UIImage? = UIImage() {
        didSet {
            if let image = vehicleImage { self.vehicleImageView.image = image }
        }
    }
    
    /// When this delegate is set, listeners are attatched to the buttons which, when tapped call this deleagate,
    /// passing in the views' tag
    public var delegate: DashboardVehicleCellDelegate? = nil {
        didSet {
            if delegate != nil {
                viewVehicleButton.addTarget(self, action: #selector(viewVehicleButtonTouched(_:)), for: .touchUpInside)
                removeVehicleButton.addTarget(self, action: #selector(removeVehicleButtonTouched(_:)), for: .touchUpInside)
            }
        }
    }
    
    // Setup the the subview when view is moved inside another view
    override func didMoveToSuperview() {
        setupView()
    }
    
    // Called when the view is added to another view. Sets up the view and its' subviews layout constrinats using Snapkit.
    private func setupView() {
        self.layer.cornerRadius = 8.0
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.clear.cgColor
        self.layer.masksToBounds = true
        self.backgroundColor = UIColor(hex: Global.colour.darkGrey)
        
        addSubview(contentContainerView)
        contentContainerView.addSubview(vehicleImageView)
        contentContainerView.addSubview(labelContainerView)
        labelContainerView.addSubview(vehicleMakeModelLabel)
        labelContainerView.addSubview(vehicleColourLabel)
        labelContainerView.addSubview(vehicleRegistrationLabel)
        addSubview(contentLineSeperator)
        addSubview(viewVehicleButton)
        addSubview(removeVehicleButton)
        
        
        contentContainerView.snp.makeConstraints({(make) -> Void in
            make.top.left.right.equalToSuperview()
            make.height.equalTo(120)
        })
        
        vehicleImageView.snp.makeConstraints({(make) -> Void in
            make.top.left.equalToSuperview().offset(16)
            make.bottom.equalToSuperview().offset(-8)
            make.width.equalTo(vehicleImageView.snp.height)
        })
        
        labelContainerView.snp.makeConstraints({(make) -> Void in
            make.top.equalToSuperview().offset(16)
            make.left.equalTo(vehicleImageView.snp.right).offset(4)
            make.right.equalToSuperview().offset(-16)
            make.bottom.equalToSuperview().offset(-8)
        })
        
        vehicleMakeModelLabel.snp.makeConstraints({(make) -> Void in
            make.right.top.left.right.equalToSuperview()
            make.bottom.equalTo(vehicleColourLabel.snp.top)
        })
        
        vehicleColourLabel.snp.makeConstraints({(make) -> Void in
            make.centerX.centerY.left.right.equalToSuperview()
            make.height.equalToSuperview().dividedBy(3)
        })
        
        vehicleRegistrationLabel.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(vehicleColourLabel.snp.bottom)
            make.left.right.bottom.equalToSuperview()
        })
        
        contentLineSeperator.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(contentContainerView.snp.bottom)
            make.left.right.equalToSuperview()
            make.height.equalTo(1)
        })
        
        viewVehicleButton.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(contentLineSeperator.snp.bottom).offset(8)
            make.left.equalToSuperview().offset(16)
            make.right.equalTo(self.snp.centerX).offset(-8)
            make.height.equalTo(35)
        })
        
        removeVehicleButton.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(contentLineSeperator.snp.bottom).offset(8)
            make.left.equalTo(self.snp.centerX).offset(8)
            make.right.equalToSuperview().offset(-16)
            make.height.equalTo(35)
        })
    }
}


// The delegate methods that are called relative to which button is tapped
extension DashboardVehicleCell {
    @objc func viewVehicleButtonTouched(_ sender: Any) {
        self.delegate?.viewVehicleButtonTouched(viewTag: self.tag)
    }
    
    @objc func removeVehicleButtonTouched(_ sender: Any) {
        self.delegate?.removeVehicleButtonTouched(viewTag: self.tag)
    }
}
