//
//  NoVehiclesView.swift
//  Share My Journey
//
//  Created by Robert John Britton on 11/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit

class NoVehiclesInfoView: UIView {
    
    // The label used to display information to the user
    private let informationTextView: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .center
        label.sizeToFit()
        label.text = Global.informationText.noVehiclesRegistered
        return label
    }()
    
    // Setup the view during initialisation
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.layer.cornerRadius = 8.0
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor(hex: Global.colour.darkGrey).cgColor
        self.layer.masksToBounds = true

        // Creates an inset that is applied to the information label
        let labelPadding = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
        
        addSubview(informationTextView)
        informationTextView.snp.makeConstraints({(make) -> Void in
            make.edges.equalToSuperview().inset(labelPadding)
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
