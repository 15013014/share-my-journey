//
//  DashboardProfileController.swift
//  Share My Journey
//
//  Created by Robert John Britton on 05/03/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit
import Firebase


class DashboardProfileController: UIViewController {
    var profileView: DashboardProfileView!
    
    // Create a user profile model object
    private var userProfile: UserProfile = {
        return UserProfile()
    }()
    
    
    // Create a reference to the root of the database
    var ref: DatabaseReference = {
        return Database.database().reference()
    }()

    let profileTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Your Profile"
        return label
    }()
    
    let yourProfileLineSeperator: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(hex: Global.colour.lightGrey)
        return view
    }()
    
    let vehiclesTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Your Vehicles"
        return label
    }()
    
    let yourVehiclesLineSeperator: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(hex: Global.colour.lightGrey)
        return view
    }()
    
    // Declare the child view controller that will display the list of user vehicles
    var dashboardVehiclesViewController: DashboardVehiclesController!
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nil, bundle: nil)
        
        // Initialise the child view controller
        dashboardVehiclesViewController = DashboardVehiclesController(collectionViewLayout: UICollectionViewFlowLayout())
        
        ref = Database.database().reference()
        userProfile = UserProfile()
        profileView = DashboardProfileView()
        
        
        addChildViewController(dashboardVehiclesViewController)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupTitleLabel()
        
        // Add a handler to deal with the user profile edit button tap
        profileView.editProfileButton.addTarget(self, action: #selector(handleEditButtonTouch), for: .touchUpInside)
        
        dashboardVehiclesViewController.didMove(toParentViewController: self)
    }
    
    // Shows the current user profile information as the view appears, this allows for changes to the profile to be shown immediately
    override func viewDidAppear(_ animated: Bool) {
        fetchUserDashboardProfile()
    }

    // Present the user with their full profile, which they may use to add and edit their details
    @objc private func handleEditButtonTouch() {
        let userPrivateProfile = UserPrivateProfileViewController()
        
        // Set the userProfileObject for the private profile here. So that the info is available to the private profile
        userPrivateProfile.userProfile = userProfile
        
        let navigationController = UINavigationController(rootViewController: userPrivateProfile)
        present(navigationController, animated: true, completion: nil)

    }
    
    
    // Fetches and displays the signed-in users' profile details obtained from the database
    func fetchUserDashboardProfile() {
        // Get the user that is currently signed in
        guard let user = Auth.auth().currentUser else { return }
        
        
        // Create reference to the root of the 'users' node
        let userQuery = ref.child("\(Global.databaseKeys.users.parentNode)/\(user.uid)")
        
        userQuery.observeSingleEvent(of: .value, with: {(snapshot) in
            
            // Store the values from the retrieved firebase snapshot
            if let dictionary = snapshot.value as? [String: AnyObject] {
                self.userProfile.userId = dictionary["userId"] as! String
                self.userProfile.username = dictionary["username"] as! String
                self.userProfile.firstName = dictionary["firstName"] as! String
                self.userProfile.lastName = dictionary["lastName"] as! String
                self.userProfile.gender = dictionary["gender"] as! String
                self.userProfile.dateOfBirth = dictionary["dateOfBirth"] as! Int
                self.userProfile.joined = dictionary["joined"] as! Int
                self.userProfile.bio = dictionary["bio"] as! String
                self.userProfile.profileImageURL = dictionary["profileImageURL"] as! String
            }
            
            // Format the user joined date ready to place in display label
            let joinedDateFormatted = FormatDate.formatIntegerTimeIntervalToDateString(timeInterval: self.userProfile.joined)
            
            self.profileView.usernameLabel.text = ("Username: \(self.userProfile.username)")
            self.profileView.userJoinedLabel.text = ("Joined: \(joinedDateFormatted)")

    
            // Display a user profile image appropriate to the users' gender
            if self.userProfile.gender == "Male" {
                self.userProfile.profileImageURL = "user_profile_male"
                self.profileView.profileImage.image = UIImage(named: self.userProfile.profileImageURL)
            } else {
                self.userProfile.profileImageURL = "user_profile_female"
                self.profileView.profileImage.image = UIImage(named: self.userProfile.profileImageURL)
            }
        })
    }
        
    private func setupTitleLabel() {
        view.addSubview(profileTitleLabel)
        view.addSubview(profileView)
        view.addSubview(yourProfileLineSeperator)
        view.addSubview(vehiclesTitleLabel)
        view.addSubview(yourVehiclesLineSeperator)
        view.addSubview(dashboardVehiclesViewController.view)
        
        
        profileTitleLabel.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.left.equalTo(view.safeAreaLayoutGuide.snp.left).offset(16)
            make.right.equalTo(view.safeAreaLayoutGuide.snp.right).offset(-16)
            make.height.equalTo(30)
        })
        
        yourProfileLineSeperator.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(profileTitleLabel.snp.bottom)
            make.left.right.equalTo(profileTitleLabel)
            make.height.equalTo(1)
        })
        
        profileView.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(yourProfileLineSeperator.snp.bottom)
            make.left.right.equalTo(profileTitleLabel)
            make.height.equalTo(140)
        })
        
        vehiclesTitleLabel.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(profileView.snp.bottom).offset(16)
            make.left.right.height.equalTo(profileTitleLabel)
        })
        
        yourVehiclesLineSeperator.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(vehiclesTitleLabel.snp.bottom)
            make.left.right.equalToSuperview()
            make.height.equalTo(1)
        })
        
        dashboardVehiclesViewController.view.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(yourVehiclesLineSeperator.snp.bottom)
            make.left.right.equalToSuperview()
            make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
        })
    }
    
}
