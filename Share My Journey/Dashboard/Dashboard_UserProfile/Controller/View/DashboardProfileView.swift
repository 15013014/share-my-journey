//
//  DashboardProfileView.swift
//  Share My Journey
//
//  Created by Robert John Britton on 17/03/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit


class DashboardProfileView: UIView {
    
    let contentContainerView: UIView = {
        return UIView()
    }()
    
    let profileImage: UIImageView = {
        let img = UIImage()
        let iv = UIImageView(image: img)
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    
    let usernameLabel: UILabel = {
        let label = UILabel()
        label.text = "Username:"
        label.textAlignment = .center
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    let userJoinedLabel: UILabel = {
        let label = UILabel()
        label.text = "Joined:"
        label.textAlignment = .center
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    let editProfileButton: UIButton = {
        let button = UIButton().defaultButton(title: Global.titleText.button.editProfile)
        return button
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor(hex: Global.colour.white)
        setupView()
    }
    
    private func setupView() {
        addSubview(contentContainerView)
        contentContainerView.addSubview(profileImage)
        contentContainerView.addSubview(usernameLabel)
        contentContainerView.addSubview(userJoinedLabel)
        addSubview(editProfileButton)
        
        
        contentContainerView.snp.makeConstraints({(make) -> Void in
            make.top.left.right.equalToSuperview()
            make.height.equalTo(100)
        })
        
        profileImage.snp.makeConstraints({(make) -> Void in
            make.centerY.left.equalToSuperview()
            make.width.height.equalTo(75)
        })
        
        usernameLabel.snp.makeConstraints({(make) -> Void in
            make.bottom.equalTo(contentContainerView.snp.centerY)
            make.right.equalToSuperview()
            make.left.equalTo(profileImage.snp.right)
            make.height.equalTo(30)
        })
        
        userJoinedLabel.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(usernameLabel.snp.bottom)
            make.left.equalTo(profileImage.snp.right)
            make.right.equalToSuperview()
            make.height.equalTo(30)
        })
        
        editProfileButton.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(contentContainerView.snp.bottom)
            make.left.right.equalToSuperview()
            make.height.equalTo(40)
            make.bottom.equalToSuperview()
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

