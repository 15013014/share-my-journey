//
//  JourneyMatchCell.swift
//  Share My Journey
//
//  Created by Robert John Britton on 20/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit

class JourneyMatchCell: PlannedJourneyCell {
    
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        arrivingDateLabel.textColor = UIColor(hex: Global.colour.green)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
