//
//  JourneyMatchesHeaderView.swift
//  Share My Journey
//
//  Created by Robert John Britton on 20/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit

class JourneyMatchesHeaderView: PlannedJourneyHeaderView {

    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        headerTitle.text = "Journey Matches"
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
