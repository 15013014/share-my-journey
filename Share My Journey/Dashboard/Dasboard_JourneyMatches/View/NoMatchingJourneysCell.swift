//
//  NoMatchingJourneysCell.swift
//  Share My Journey
//
//  Created by Robert John Britton on 20/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit

class NoMatchingJourneysCell: NoPlannedJourneyCell {

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        infoLabel.text = """
            No Matches Found.\n\n
            When a matching journey is found, it will appear here.\n\n
            You may pull down this page to refresh it.
            """
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
