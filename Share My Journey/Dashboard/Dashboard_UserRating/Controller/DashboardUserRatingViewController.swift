//
//  UserRatingViewController.swift
//  Share My Journey
//
//  Created by Robert John Britton on 18/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit

class DashboardUserRatingViewController: UIViewController {

    let userInfoLabel: UILabel = {
        let label = UILabel()
        label.text = """
                        This feature is currently undergoing development and will be available soon.
                      """
        label.textAlignment = .center
        label.numberOfLines = 4
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.white
        
        view.addSubview(userInfoLabel)
        userInfoLabel.snp.makeConstraints({(make) -> Void in
            make.centerX.centerY.equalToSuperview()
            make.width.equalToSuperview().offset(-32)
        })
    }
    
}
