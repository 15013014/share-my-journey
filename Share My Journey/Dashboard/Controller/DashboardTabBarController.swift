//
//  UserDashboardTabBarController.swift
//  Share My Journey
//
//  Created by Robert John Britton on 05/03/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit
import Firebase


class DashboardTabBarController: UITabBarController, UITabBarControllerDelegate {

    // Declare a variable to hold the ref to db root
    var ref: DatabaseReference?
    
    // Declare the observer for the vehicles node in db
    var vehiclesHandle: DatabaseHandle?
    
    
    
    // Initialise the dashboard profile view controller and set the view controllers' tabBar item title and icon.
    // These tab bar items become visible in the bottom tab bar once added to the viewControllers array.
    private let dashboardProfileController: DashboardProfileController = {
        let viewController = DashboardProfileController()
        viewController.tabBarItem.title = "Profile"
        viewController.tabBarItem.image = UIImage(named: "profile_tab_icon")
        return viewController
    }()
    
    // Initialise the dashboard journeys view controller and set the view controllers' tabBar item title and icon.
    // These tab bar items become visible in the bottom tab bar once added to the viewControllers array.
    private let plannedJourneysController: DashboardPlannedJourneysViewController = {
        let viewController = DashboardPlannedJourneysViewController()
        viewController.tabBarItem.title = "Journeys"
        viewController.tabBarItem.image = UIImage(named: "journey_tab_icon")
        return viewController
    }()
    
    // Initialise the dashboard user ratings view controller and set the view controllers' tabBar item title and icon.
    // These tab bar items become visible in the bottom tab bar once added to the viewControllers array.
    private let userRatingViewController: DashboardUserRatingViewController = {
        let viewController = DashboardUserRatingViewController()
        viewController.tabBarItem.title = "Feedback"
        viewController.tabBarItem.image = UIImage(named: "rate_tab_icon")
        return viewController
    }()
    
    // Initialise the dashboard journey matches view controller and set the view controllers' tabBar item title and icon.
    // These tab bar items become visible in the bottom tab bar once added to the viewControllers array.
    private let journeyMatchesViewController: DashboardJourneyMatchesViewController = {
        let viewController = DashboardJourneyMatchesViewController()
        viewController.tabBarItem.title = "Matches"
        viewController.tabBarItem.image = UIImage(named: "match_tab_icon")
        return viewController
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Check the users journeys that are stored in the database once this controller has finished loading.
        // This removes completed journeys from the journeys node in the database along with any references to it,
        // then add the journey data to a completed journeys node. Completed journey data will be used in the user rating view controller, where the user can select a completed journey, and provided feedback for driver/passengers of the journey.
        clearExpiredUserJourneys()
        
        // Setup the navigation bar title, and buttons.
        navigationItem.title = "Dashboard"
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Sign Out", style: UIBarButtonItemStyle.plain, target: self, action: #selector(signOut))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Messages", style: UIBarButtonItemStyle.plain, target: self, action: nil)
        
        // Placeholder for the requests view controller. Will be implemened at some point in the future.
        let vc3 = UIViewController()
        vc3.tabBarItem.title = "Requests"
        vc3.tabBarItem.image = UIImage(named: "request_tab_icon")
        
        // Add the initialised view controllers to the array in the order we want them to appear in the tab bar.
        viewControllers = [dashboardProfileController, plannedJourneysController, userRatingViewController, journeyMatchesViewController, vc3]
    }
    
    // Close the connection to Firebase authentication. Dismiss the navigation controller/ tab bar controller which returns user to the sign in view controller.
    @objc private func signOut(){
        // If Firebase session exists, end session.
        do {
            try Auth.auth().signOut()
        } catch let signOutError as NSError {
            print("Error signing out %@", signOutError)
            return
        }
        
        // Dismiss the dashboard view controller, bringing the user back to the sign in view controller
        dismiss(animated: false, completion: nil)
    }
    
    
    // Removes all journeys belonging to the user from the database where the journey end date/time is less than the current date/time
    // Ideally this would be done server side, as this relies on the user to log in to keep the database fully in sync.
    private func clearExpiredUserJourneys() {
        
        if let userId = Auth.auth().currentUser?.uid {
            
            let ref = Database.database().reference()
            let journeysRef = ref.child("journeys")
            
            // Set the reference point to the user_journeys node in the database. This node contains a list of userchild nodes
            let userJourneysRef = ref.child("user_journeys")
            
            // Set the reference point at the journey_start_city_area, a parent node that contains a list of userId child nodes.
            // Under each child node, is a list of references to the users journeys (which exists in the key) and the journey end date (stored as the value).
            let journeyStartCityAreaRef = ref.child("journey_start_city_area")
            
            // Obtain the list of journeys that belong to the current user
            userJourneysRef.child(userId).observeSingleEvent(of: .value) { (snapshot) in
                
                for child in snapshot.children {
                    let snap = child as! DataSnapshot
                    
                    // Obtain the date (in integer format) from the snapshot.value (we stored the end date here during journey creation)
                    if let journeyEndDate = (snap.value as? Int) {
                        print(journeyEndDate)
                        
                        // Get the current date/time
                        let date = Date()
                        
                        // Convert the date we obtained from the snapshot and convert it into Date format
                        let endDate = Date(timeIntervalSince1970: Double(journeyEndDate))
                        
                        // Check that the end time/date is less than the current time/date
                        if endDate < date {
                            
                            // Where the journey end date is less, obtain the journey from the journeys node (we use the snapshot key to identify the journey to pull from this node)
                            journeysRef.child("\(snap.key)").observeSingleEvent(of: .value, with: { (snapshot) in
                                
                                if let dictionary = snapshot.value as? [String: AnyObject] {
                                    guard let startCity = dictionary["startCity"] as? String else { return }
                                    guard let startArea = dictionary["startArea"] as? String else { return }
                                    
                                    // Now copy the completed journey to the completed_journeys node, under the users id. Give the data a parent node with the same key it had before, (could have used autoID but prefer to reuse keys)
                                    ref.child("user_completed_journeys/\(userId)/\(snap.key)").setValue(snapshot.value)
                                    
                                    // Remove the completed journey and references to it from the database
                                    // The journey will no longer appear in the users list of planned journeys.
                                    // Rather will appear in the users list of completed journeys (once implemented)
                                    journeyStartCityAreaRef.child("\(startCity)_\(startArea)/\(snap.key)").removeValue()
                                    userJourneysRef.child("\(userId)/\(snap.key)").removeValue()
                                    journeysRef.child("\(snap.key)").removeValue()
                                }
                            })
                        }
                    }
                }
            }
        }
    }
    
    // Observe the vehicles node -----------------------------------------------------------------------------------------------------------
    private func observeDatabase() {
        
        guard let ref = ref else {
            fatalError("Database Reference Undefined")
        }
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        let vehiclesRef = ref.child("user_vehicles/\(uid)")
            
            // observes changes to the user vehicles, pulling references from the node som we can find the
            // vehicle information in the vehicles node
            vehiclesHandle = vehiclesRef.observe(.childAdded, with: {(snapshot) in
                
                var vehicles: [UserVehicle] = [UserVehicle]()
                
                for child in snapshot.children {
                    let snap = child as! DataSnapshot
                    let vehicleKey = snap.key
                    
                    let vehiclesRef = Database.database().reference().child("vehicles/\(vehicleKey)")
                    
                    // Pulls vehicle information, whic is identified by the reference from the query above
                    // Creates a user vehicle model which is added to a temp array. Once all vehicles have been
                    // pulled, set the vehicle array in view controller, which reloads the collection view
                    vehiclesRef.observeSingleEvent(of: .value, with: {(vehicleSnapshot) in
                        
                        // Create a dictionary to store the key:value JSon data from database
                        if let dictionary = vehicleSnapshot.value as? [String: AnyObject] {
                            
                            let userVehicle = UserVehicle()
                            userVehicle.colour = dictionary["colour"] as! String
                            userVehicle.make = dictionary["make"] as! String
                            userVehicle.model = dictionary["model"] as! String
                            userVehicle.passengerSeats = dictionary["passengerSeats"] as! Int
                            userVehicle.registration = dictionary["registration"] as! String
                            userVehicle.userId = dictionary["userId"] as! String
                            userVehicle.vehicleId = dictionary["vehicleId"] as! String
                            
                            // Append each new vehicle object the temporary array
                            vehicles.append(userVehicle)
                        }
                        
                        // Setting the array here, instantly updates the collectionView with all the vehicles
//                        self.vehicle = vehicles
                    })
                }
            })
        
        
    }
    
    // Create an observer for a given database reference point
//    private func addDatabaseObserver(ref: DatabaseReference) -> DatabaseHandle {
//
//    }
    
    private func removeDatabaseObserver(ref: DatabaseReference, handle: DatabaseHandle) -> Bool {
        return true
    }
}
