//
//  UserVehicle.swift
//  Share My Journey
//
//  Created by Robert John Britton on 09/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit

class UserVehicle: NSObject {
    var make:           String  = "" // The manufacturer of the vehicle
    var model:          String  = "" // The model
    var colour:         String  = ""
    var passengerSeats: Int     = 0  // The number of free passenger seats available
    var registration:   String  = "" // Vehicles registration number
    var vehicleId:      String  = "" // The vehicles unique id
    var userId:         String  = "" // The owners user id
}
