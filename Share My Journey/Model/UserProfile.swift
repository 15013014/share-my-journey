//
//  User.swift
//  Share My Journey
//
//  Created by Robert John Britton on 12/02/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit

class UserProfile: NSObject {
    var userId:          String  = ""
    var username:        String  = ""
    var firstName:       String  = ""
    var lastName:        String  = ""
    var gender:          String  = ""
    var dateOfBirth:     Int     = 0
    var bio:             String  = ""
    var joined:          Int     = 0
    var profileImageURL: String  = ""
}
