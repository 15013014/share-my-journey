//
//  UserJourney.swift
//  Share My Journey
//
//  Created by Robert John Britton on 17/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit

class UserJourney: NSObject {
    var journeyName: String?        // The name given to the journey by the creator
    var driverId: String?           // Used to identify the user that created the journey
    var vehicleId: String?          // Used to identify the vehicle
    var journeyId: String?
    var startDate: Int?             // The date and time the journey begins
    var startCity: String?          // The location where the journey begins
    var startArea: String?
    var endDate: Int?               // The date and time the journey ends
    var endCity: String?            // The journey destination
    var endArea: String?
    var maxPassengers: Int?         // The numbeer of passengers permitted, set by the user and no more
                                    // than the number of passenger seats available for the chosen vehicle
    var pickupInfo: String?         // Describes the exact location where the driver will meet passengers
    var passengers = [String?]()    // The list of passengers that joined the journey
}
