//
//  Extensions.swift
//  Share My Journey
//
//  Created by Robert John Britton on 16/11/2017.
//  Copyright © 2017 Robert John Britton. All rights reserved.
//

import UIKit

// An extension to make presenting a custom alert easier and cleaner
extension UIViewController {
    func presentAlert(title: String?, message: String) {
        let alert = UIAlertController(title: Global.alertMsg.defaultAlertTitle, message: message, preferredStyle: .alert)
        if let title = title {
            alert.title = title
        }
        alert.addAction(UIAlertAction(title: Global.titleText.button.dismiss, style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}



extension UIButton {
    public func defaultButton(title: String) -> UIButton {
        let button = UIButton(type: UIButtonType.system)
        button.backgroundColor = UIColor(hex: Global.colour.darkBlue)
        button.setTitle(title, for: .normal)
        button.layer.cornerRadius = 8
        button.clipsToBounds = true
        button.setTitleColor(UIColor(hex: Global.colour.white), for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        return button
    }
    
    public func successButton(title: String) -> UIButton {
        let button = defaultButton(title: title)
        button.backgroundColor = UIColor(hex: Global.colour.green)
        return button
    }
    
    public func warningButton(title: String) -> UIButton {
        let button = defaultButton(title: title)
        button.backgroundColor = UIColor(hex: Global.colour.red)
        return button
    }
}


extension Date {
    // Returns the amount of years from another date
    func years(date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    
    // Returns the a custom time interval description from another date
    func offset(date: Date) -> String {
        if years(date: date) > 0 {
            return "\(years(date: date))"
        }
        return ""
    }
}
