//
//  SignInViewController+Handlers.swift
//  Share My Journey
//
//  Created by Robert John Britton on 12/02/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit

extension SignInViewController {
    // Force-end any editing / Resign on-screen keyboard and push the next view controller to the stack
    @objc func handleRegisterButtonTouch() {
        view.endEditing(true)
        navigationController?.pushViewController(RegistrationViewController(), animated: true)
    }
}
