//
//  enum.swift
//  Share My Journey
//
//  Created by Robert John Britton on 03/03/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit


struct Global {
    
    static let maxMinutesBetweenJourneys: Int = 30
    
    struct alertMsg {
        static let defaultAlertTitle: String        = "Alert"
        static let invalidEmail: String             = "Invalid Email"
        static let userNotFound: String             = "No user found with information provided"
        static let emailAlreadyInUse: String        = "Email already in use"
        static let invalidEmailPassword: String     = "Unable to find an account matching the email/password provided"
        static let passwordMismatch: String         = "The passwords do not match"
        static let tooManyRequests: String          = "Too many sign in requests. Please try again later"
        static let weakPassword: String             = "Password must contain at least 6 characters"
        static let accountLocked: String            = "This account is locked"
        static let emptyEmail: String               = "Email field should not be empty"
        static let emptyPassword: String            = "Password field should not be empty"
        static let emptyUsername: String            = "Username field should not be empty"
        static let emptyGender: String              = "Gender field should not be empty"
        static let emptyFirstName: String           = "First name field should not be empty"
        static let emptyLastName: String            = "Last name field should not be empty"
        static let emptyDob: String                 = "Date of birth field should not be empty"
    }
    
    /// Contains a number of CharacterSets used in conjuction with the BaseTextField (and it's subclasses) when specifying a valid range of text for a particular textField
    struct validCharSet {
        static let email: CharacterSet                  = CharacterSet(charactersIn:"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789@-_.")
        static let password: CharacterSet               = CharacterSet(charactersIn:" ").inverted
        static let textOnly: CharacterSet               = CharacterSet(charactersIn:"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
        static let numericOnly: CharacterSet            = CharacterSet(charactersIn:"0123456789")
        static let textNumericOnly: CharacterSet        = CharacterSet(charactersIn:"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
        static let textNumericWithSpace: CharacterSet   = CharacterSet(charactersIn:"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 ")
    }
    
    struct placeholder {
        static let email: String        = "Email..."
        static let password: String     = "Password..."
        static let username: String     = "Username..."
        static let firstname: String    = "First name..."
        static let lastname: String     = "Last name..."
        static let gender: String       = "Gender..."
        static let dob: String          = "Date of birth..."
    }
    
    struct titleText {
        struct button {
            static let register: String     = "Register"
            static let signin: String       = "Sign In"
            static let saveChanges: String  = "Save Changes"
            static let cancel: String       = "Cancel"
            static let editProfile: String  = "Edit Profile"
            static let dismiss: String      = "Dismiss"
            static let Continue: String     = "Continue"
        }
        
        struct view {
            static let signin: String       = "Sign In"
            static let register: String     = "Register"
            static let dashboard: String    = "Dashboard"
            static let profile: String      = "Profile"
        }
        
        struct label {
            static let username: String     = "Username:"
            static let firstname: String    = "First Name:"
            static let lastname: String     = "Last Name:"
            static let age: String          = "Age:"
            static let gender: String       = "Gender:"
            static let bio: String          = "Bio:"
            static let joined: String       = "Joined"
        }
    }
    
    struct colour {
        static let darkBlue: String         = "#007AFF"
        static let lightBlue: String        = "#2589bd"
        static let green: String            = "#4cb944"
        static let white: String            = "#ffffff"
        static let red: String              = "#ff6666"
        static let lightRed: String         = "#ffa4a4"
        static let lightLightGrey: String   = "#F7F7F7"
        static let lightGrey: String        = "#CCCCD1"
        static let darkGrey: String         = "#403f4c"
    }
    
    struct fontSize {
        static let small: CGFloat   = 12
        static let medium: CGFloat  = 14
        static let large: CGFloat   = 16
    }
    
    struct databaseKeys {
        
        struct users {
            static let parentNode: String        = "users"
            static let username: String          = "username"
            static let userId: String            = "userId"
            static let firstName: String         = "firstName"
            static let lastName: String          = "lastName"
            static let dateAdded: String         = "timestamp"
            static let gender: String            = "gender"
            static let bio: String               = "bio"
        }
        
        struct vehicles {
            static let parentNode: String       = "vehicles"
            static let colour: String           = "colour"
            static let make: String             = "make"
            static let model: String            = "model"
            static let passengerSeats: String   = "passengerSeats"
            static let registration: String     = "registration"
            static let userId: String           = "userId"
        }
    }
    
    struct informationText {
        static let noVehiclesRegistered: String =    """
                            You currently have no vehicles registered to your account.\n
                            Add a vehicle by tapping the 'Add Vehicle' button.\n
                            Note: You may have a maximum of 3 vehicles registered to your account at any time.
                            """
        static let noPlannedJourneys: String =    """
                            You currently have no planned jourenys.\n
                            Create a journey by tapping the 'Create Journey' button.\n
                            """
    }
    
}


