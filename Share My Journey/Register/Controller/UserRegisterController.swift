//
//  RegistrationViewController.swift
//  Share My Journey
//
//  Created by Robert John Britton on 29/01/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit
import Firebase



/// ## UserRegisterController
/// 1 Displays the RegisterView
/// - Creates a UserProfile model
/// - Provides GenderPicker with list of genders
/// - 
class UserRegisterController: UIViewController {

    // This items the gender pickerview delegate will display
    private let genderList = ["Male", "Female"]
    
    // The user profile model object, to store user information
    private var userProfile: UserProfile = {
        return UserProfile()
    }()
    
    private var registerView: RegisterView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerView = RegisterView()
        registerView.delegate = self
        registerView.textFieldDelegate = self
        registerView.genderPickerView.dataSource = self
        registerView.genderPickerView.delegate = self
        
        view.backgroundColor = UIColor.white

        navigationItem.title = Global.titleText.view.register
        
        setupView()
    }
    
    /// Register the keyboard observers, starts listning for keyboard events as the view will be shown.
    override func viewWillAppear(_ animated: Bool) {
        registerKeyboardObservers()
    }
    
    /// Removes the keyboard observers, as the view disappears. We know that the user will not be editing text in the view
    /// as it will be no longer accessible.
    override func viewWillDisappear(_ animated: Bool) {
        removeKeyboardObservers()
    }
    
    /// Add the RegisterView to the container view then sets its auto layout constraints top,left,righ,bottom anchors
    //// equal to the container view. This makes the size of the RegisterView equal to the container view
    private func setupView() {
        view.addSubview(registerView)
        registerView.snp.makeConstraints({(make) -> Void in
            make.edges.equalToSuperview()
        })
    }
}

// Resign the controller everytime the user finishes editing a textField (keeps the keyboard observer up to date in order for the scroll view to reposition correctly)
extension UserRegisterController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.layer.backgroundColor = UIColor.white.cgColor
        
        if let placeHolder = textField.placeholder{
            textField.attributedPlaceholder = NSAttributedString(string: placeHolder , attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        }
    }
    
    // Highlights the textfield if the user ends editing and the textfield is empty
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if let text = textField.text, text.isEmpty {
            textField.layer.backgroundColor = UIColor(hex: "#ffa4a4").cgColor
            
            if let placeHolder = textField.placeholder{
                textField.attributedPlaceholder = NSAttributedString(string: placeHolder , attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
            }
        }
        
        textField.resignFirstResponder()
        return true
    }
    
    // Dismisses the on-screen keybaord
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let text = textField.text, !text.isEmpty else { return }
        
        enum Tag: Int {
            case email = 1, password = 2, username = 3
        }
        
        textField.resignFirstResponder()
    }
}



// extends the class to include the picker view delegate methods
extension UserRegisterController: UIPickerViewDelegate, UIPickerViewDataSource {
    // The following methods setup the pickerView, to display the male and female options available for selsection by the user.
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return genderList.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return genderList[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.userProfile.gender = genderList[row]

    }
}



// extends the class to include the button interactions
extension UserRegisterController: RegisterViewDelegate {
    func registerButtonTapped() {
        // Check all fields contain a string value
        userProfile.username = registerView.usernameTextField.text!
        userProfile.gender = registerView.genderTextField.text!
        userProfile.firstName = registerView.firstNameTextField.text!
        userProfile.lastName = registerView.lastNameTextField.text!
        
        ////////////////// todo, tidy up
        if registerView.emailTextField.text!.isEmpty { presentAlert(title: nil, message: Global.alertMsg.emptyEmail); return }
        if registerView.passwordTextField.text!.isEmpty { presentAlert(title: nil, message: Global.alertMsg.emptyPassword); return  }
        if userProfile.username.isEmpty { presentAlert(title: nil, message: Global.alertMsg.emptyEmail); return }
        if userProfile.gender.isEmpty { presentAlert(title: nil, message: Global.alertMsg.emptyGender); return }
        if userProfile.firstName.isEmpty { presentAlert(title: nil, message: Global.alertMsg.emptyFirstName); return }
        if userProfile.lastName.isEmpty { presentAlert(title: nil, message: Global.alertMsg.emptyLastName); return }
        if registerView.dateOfBirthTextField.text!.isEmpty { presentAlert(title: nil, message: Global.alertMsg.emptyDob); return }
        
        // Get the exact date from the date picker and store in user profile model object
        let dateToInt =  FormatDate.formateDateToIntegerTimeInterval(date: registerView.dateOfBirthDatePickerView.date)
        userProfile.dateOfBirth = dateToInt
        
        // Attempt to register user account and update add the users info to Firebase
        registerUserAndUpdateProfile()
    }
    
    func generPickerDoneButtonTapped() {
        let selectedGenderIndex = registerView.genderPickerView.selectedRow(inComponent: 0)
        registerView.genderTextField.text = genderList[selectedGenderIndex]
        registerView.genderTextField.resignFirstResponder()
    }
    
    func dateOfBirthPickerDoneButtonTapped() {
        let dateToString = FormatDate.formatDateToString(date: registerView.dateOfBirthDatePickerView.date)
        registerView.dateOfBirthTextField.text = dateToString
        registerView.dateOfBirthTextField.resignFirstResponder()
    }
}



// extends the class to include Firebase interactions
extension UserRegisterController {
    
    /// Calls the Firebase Auth method, passing in the email and password entered into the textfields.
    /// Check if theres any error returned from Firebase. Present an error message if there's and error.
    /// If there's no error,
    private func registerUserAndUpdateProfile() {
        
        
        // Create new user and sign in to return user object (or error if unsuccessful)
        Auth.auth().createUserAndRetrieveData(withEmail: registerView.emailTextField.text!, password: registerView.passwordTextField.text!, completion: {(user, error) in
            if let error = error {
                // Get the error code and pass to error handler for presentation then return
                let authErrorCode = AuthErrorCode(rawValue: error._code)!
                FirebaseError.Auth.present(authErrorCode, from: self)
                return
            }
            
            // If no error, get the newly created user object
            guard let user = user?.user else { return }
            
            // Get the users Id
            let userId = user.uid
            
            
            // Create a reference to the root of the database
            let ref = Database.database().reference()
            
            // Get the current date of account creation to store in table
            let timestamp = Int(Date().timeIntervalSince1970)
            self.userProfile.joined = timestamp
            
            let post: Any = [
                "userId":"\(userId)",
                "username":"\(self.userProfile.username)",
                "firstName":"\(self.userProfile.firstName)",
                "lastName":"\(self.userProfile.lastName)",
                "gender":"\(self.userProfile.gender)",
                "dateOfBirth":self.userProfile.dateOfBirth,
                "bio":self.userProfile.bio,
                "joined": self.userProfile.joined,
                "profileImageURL":""
            ];
            
            ref.child("users/\(userId)").setValue(post)
            
            print(post)
            
            // Update the users profile object to include the users id, returned from firebase
            self.userProfile.userId = userId
            

            // Dismiss the current view controller, taking user to the sign in page
            self.navigationController?.popViewController(animated: true)
        })
    }
}


/// Class extension that handles the scrollView position when the on-screen keyboard is visible
extension UserRegisterController  {
    func registerKeyboardObservers() {
        
        // Fires the registered method whenever the keyboard is about to hide
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        // Fires the registered method whenever the keyboard's fram changes, i.e changes from text input to a picker
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidChangeFrame), name:NSNotification.Name.UIKeyboardDidChangeFrame, object: nil)
    }
    
    func removeKeyboardObservers() {
        NotificationCenter.default.removeObserver(self, name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.removeObserver(self, name:NSNotification.Name.UIKeyboardDidChangeFrame, object: nil)
    }
    
    @objc func keyboardDidChangeFrame(notification:NSNotification){
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        // Gets the current edge insets of the scrollView
        var contentInset:UIEdgeInsets = registerView.scrollView.contentInset
        
        // Update the bottom inset so that includes the height of the visible keyboard
        contentInset.bottom = keyboardFrame.size.height
        
        // Replace the scrollViews contentInsets with the updated insets that include the bottom keyboard height
        // This gives the ability for the scrollView to scroll to the text field behind the keyboard.
        registerView.scrollView.contentInset = contentInset
    }
    
    // Triggered by the UIKeyboardWillHide observer when the keyboard is about to hide.
    // Removes the bottom inset from the main container
    @objc func keyboardWillHide(notification:NSNotification){
        // Create a zero content inset object and assign it to the scrollView.
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        registerView.scrollView.contentInset = contentInset
    }
}
