//
//  RegisterView.swift
//  Share My Journey
//
//  Created by Robert John Britton on 14/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit


protocol RegisterViewDelegate: UITextFieldDelegate {
    func registerButtonTapped()
    func generPickerDoneButtonTapped()
    func dateOfBirthPickerDoneButtonTapped()
}

class RegisterView: UIView {

    var delegate: RegisterViewDelegate?
    
    // Setting thie delegate sets all of the required textField delagtes
    var textFieldDelegate: UITextFieldDelegate? {
        didSet {
            self.emailTextField.delegate = textFieldDelegate
            self.passwordTextField.delegate = textFieldDelegate
            self.genderTextField.delegate = textFieldDelegate
            self.usernameTextField.delegate = textFieldDelegate
            self.firstNameTextField.delegate = textFieldDelegate
            self.lastNameTextField.delegate = textFieldDelegate
            self.dateOfBirthTextField.delegate = textFieldDelegate
        }
    }
        
    // -- SUBTITLE
    private let viewSubtitleLabel : UILabel = {
        let label = UILabel()
        label.text = "Account Information:"
        label.textColor = UIColor(hex: Global.colour.darkGrey)
        label.font = UIFont.systemFont(ofSize: 16)
        return label
    }()
    
    // CONTAINER VIEW TO HOLD ALL INPUT RELATED FIELDS
    private let textFieldContainerView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 10.0
        view.layer.borderWidth = 1.0
        view.layer.borderColor = UIColor(hex: Global.colour.lightGrey).cgColor
        view.clipsToBounds = true
        return view
    }()
    
    // -- EMAIL
    let emailTextField: BaseTextField = {
        let textField = BaseTextField()
        textField.validCharacters = Global.validCharSet.email
        textField.placeholder = Global.placeholder.email
        textField.keyboardType = .emailAddress
        return textField
    }()
    
    /// Displays a visible seperation between input fields
    private let emailLineSeperator: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(hex: Global.colour.lightGrey)
        return view
    }()
    
    // -- PASSWORD
    let passwordTextField: BaseTextField = {
        let textField = BaseTextField()
        textField.validCharacters = Global.validCharSet.password
        textField.placeholder = Global.placeholder.password
        textField.isSecureTextEntry = true
        return textField
    }()
    
    /// Displays a visible seperation between input fields
    private let passwordLineSeperator: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(hex: Global.colour.lightGrey)
        return view
    }()
    
    // -- USERNAME
    let usernameTextField: UITextField = {
        let textField = BaseTextField()
        textField.placeholder = Global.placeholder.username
        return textField
    }()
    
    /// Displays a visible seperation between input fields
    private let usernameLineSeperator: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(hex: Global.colour.lightGrey)
        return view
    }()
    
    
    // -- GENDER + PICKER VIEW & TOOLBAR
    private var genderTextFieldToolbar: UIToolbar = {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let flexSpaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let donButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(genderTextFieldToolbarDoneButtonTouched))
        toolbar.setItems([flexSpaceButton, flexSpaceButton, donButton], animated: false)
        return toolbar
    }()
    
    
    let genderPickerView: UIPickerView = {
        let pickerView = UIPickerView()
        return pickerView
    }()
    
    lazy var genderTextField: UITextField = {
        let textField = SelectionTextField()
        textField.placeholder = Global.placeholder.gender
        textField.inputAccessoryView = self.genderTextFieldToolbar
        textField.inputView = self.genderPickerView
        return textField
    }()
    
    /// Displays a visible seperation between input fields
    private let genderLineSeperator: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(hex: Global.colour.lightGrey)
        return view
    }()
    
    
    // -- FIRST NAME
    let firstNameTextField: UITextField = {
        let textField = BaseTextField()
        textField.validCharacters = Global.validCharSet.textOnly
        textField.placeholder = Global.placeholder.firstname
        return textField
    }()
    
    /// Displays a visible seperation between input fields
    private let firstNameLineSeperator: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(hex: Global.colour.lightGrey)
        return view
    }()
    
    // -- LAST NAME
    let lastNameTextField: UITextField = {
        let textField = BaseTextField()
        textField.validCharacters = Global.validCharSet.textOnly
        textField.placeholder = Global.placeholder.lastname
        return textField
    }()
    
    /// Displays a visible seperation between input fields
    private let lastNameLineSeperator: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(hex: Global.colour.lightGrey)
        return view
    }()
    
    
    /// Provides a toolbar above the picker which shows a 'done' button the user can tap.
    let dateOfBirthTextFieldToolbar: UIToolbar = {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let flexSpaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let donButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dateOfBirthTextFieldToolBarDoneButtonTouched))
        toolbar.setItems([flexSpaceButton, flexSpaceButton, donButton], animated: false)
        toolbar.isUserInteractionEnabled = true
        return toolbar
    }()
    

    /// Limits the selectable range between a minimum of 18 years from today and a maximum of 150 years from now. This measn that the user must be between 18 and 150 to create an account.
    let dateOfBirthDatePickerView: UIDatePicker = {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        let calenderGregorian = Calendar(identifier: .gregorian)
        let currentDate = Date()
        var dateComponents = DateComponents()
        dateComponents.year = -150
        let minSelectableDate = calenderGregorian.date(byAdding: dateComponents, to: currentDate)
        dateComponents.year = -18
        let maxSelectableDate = calenderGregorian.date(byAdding: dateComponents, to: currentDate)
        datePicker.minimumDate = minSelectableDate
        datePicker.maximumDate = maxSelectableDate
        return datePicker
    }()
    
    lazy var dateOfBirthTextField: UITextField = {
        let textField = SelectionTextField()
        textField.placeholder = Global.placeholder.dob
        textField.inputAccessoryView = self.dateOfBirthTextFieldToolbar
        textField.inputView = self.dateOfBirthDatePickerView
        return textField
    }()
    
    // -- REGISTER BUTTON
    let registerButton: UIButton = {
        let button = UIButton().defaultButton(title: Global.titleText.button.register)
        button.addTarget(self, action: #selector(handleRegisterButtonTouch), for: .touchUpInside)
        return button
    }()
    
    // SCROLLABLE VIEW THAT WRAPS AROUND ALL CONTENT
    let scrollView: UIScrollView = {
        let view = UIScrollView()
        view.isScrollEnabled = true
        return view
    }()
    
    let contentView: UIView = {
        let view = UIView()
        return view
    }()
    
    override func didMoveToSuperview() {
        setupView()
    }
    
    private func setupView() {
        // Add subviews
        addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(viewSubtitleLabel)
        contentView.addSubview(textFieldContainerView)
        textFieldContainerView.addSubview(emailTextField)
        textFieldContainerView.addSubview(emailLineSeperator)
        textFieldContainerView.addSubview(passwordTextField)
        textFieldContainerView.addSubview(passwordLineSeperator)
        textFieldContainerView.addSubview(usernameTextField)
        textFieldContainerView.addSubview(usernameLineSeperator)
        textFieldContainerView.addSubview(genderTextField)
        textFieldContainerView.addSubview(genderLineSeperator)
        textFieldContainerView.addSubview(firstNameTextField)
        textFieldContainerView.addSubview(firstNameLineSeperator)
        textFieldContainerView.addSubview(lastNameTextField)
        textFieldContainerView.addSubview(lastNameLineSeperator)
        textFieldContainerView.addSubview(dateOfBirthTextField)
        contentView.addSubview(registerButton)
        
        let inputFieldHeight = 50
        let lineSeperatorHeight = 1
        
        // Add auto layout constraints to subviews
        scrollView.snp.makeConstraints({(make) -> Void in
            make.top.left.right.bottom.equalTo(0)
        })
        
        contentView.snp.makeConstraints({(make) -> Void in
            make.top.left.right.bottom.equalTo(0)
            make.width.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(1).priority(250)
        })
        
        // Subtitle
        viewSubtitleLabel.snp.makeConstraints({(make) -> Void in
            make.top.left.equalTo(contentView.safeAreaLayoutGuide).offset(16)
            make.width.equalTo(contentView.snp.width).offset(-32)
            make.height.equalTo(30)
        })
        
        // TextField Container and content
        textFieldContainerView.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(viewSubtitleLabel.snp.bottom).offset(16)
            make.left.equalTo(contentView.safeAreaLayoutGuide.snp.left).offset(16)
            make.right.equalTo(contentView.safeAreaLayoutGuide.snp.right).offset(-16)
            make.height.equalTo(356)
        })
        
        // Email
        emailTextField.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(textFieldContainerView)
            make.left.width.equalToSuperview()
            make.height.equalTo(inputFieldHeight)
        })
        
        emailLineSeperator.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(emailTextField.snp.bottom)
            make.left.width.equalToSuperview()
            make.height.equalTo(lineSeperatorHeight)
        })
        
        // Password
        passwordTextField.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(emailLineSeperator.snp.bottom)
            make.left.width.height.equalTo(emailTextField)
        })
        
        passwordLineSeperator.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(passwordTextField.snp.bottom)
            make.left.width.height.equalTo(emailLineSeperator)
        })
        
        // Username
        usernameTextField.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(passwordLineSeperator.snp.bottom)
            make.left.width.height.equalTo(emailTextField)
        })
        
        usernameLineSeperator.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(usernameTextField.snp.bottom)
            make.left.width.height.equalTo(emailLineSeperator)
        })
        
        // Gender
        genderTextField.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(usernameLineSeperator.snp.bottom)
            make.left.width.height.equalTo(emailTextField)
        })
        
        genderLineSeperator.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(genderTextField.snp.bottom)
            make.left.width.height.equalTo(emailLineSeperator)
        })
        
        // FirstName
        firstNameTextField.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(genderLineSeperator.snp.bottom)
            make.left.width.height.equalTo(emailTextField)
        })
        
        firstNameLineSeperator.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(firstNameTextField.snp.bottom)
            make.left.width.height.equalTo(emailLineSeperator)
        })
        
        // LastName
        lastNameTextField.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(firstNameLineSeperator.snp.bottom)
            make.left.width.height.equalTo(emailTextField)
        })
        
        lastNameLineSeperator.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(lastNameTextField.snp.bottom)
            make.left.width.height.equalTo(emailLineSeperator)
        })
        
        // DateOfBirth
        dateOfBirthTextField.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(lastNameLineSeperator.snp.bottom)
            make.left.width.height.equalTo(emailTextField)
        })
        
        // Button
        registerButton.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(textFieldContainerView.snp.bottom).offset(16)
            make.left.right.equalTo(textFieldContainerView)
            make.height.equalTo(inputFieldHeight)
            make.bottom.equalTo(scrollView.snp.bottom).offset(-16)
        })
    }

    
    @objc private func genderTextFieldToolbarDoneButtonTouched() {
        self.delegate?.generPickerDoneButtonTapped()
    }
    
    @objc private func dateOfBirthTextFieldToolBarDoneButtonTouched() {
        self.delegate?.dateOfBirthPickerDoneButtonTapped()
    }
    
    @objc private func handleRegisterButtonTouch() {
        self.delegate?.registerButtonTapped()
    }
}
