//
//  ViewController.swift
//  Share My Journey
//
//  Created by Robert John Britton on 15/11/2017.
//  Copyright © 2017 Robert John Britton. All rights reserved.
//

import UIKit
import Firebase



class UserSignInController: UIViewController {
    private let passwordMinLength = 6

    private var signInView: SignInView!

    
    // When the root view has loaded, setup the navigation items and add setup subview
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.white
        
        signInView = SignInView()
        
        signInView.delegate = self
        signInView.emailTextField.delegate = self
        signInView.passwordTextField.delegate = self
        

        // Set the right navbar button action, so we can do something when the button is tapped.
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: Global.titleText.button.register, style: .plain, target: self, action: #selector(handleRegisterButtonTouch))
        
        setupContainerView()
    }
    
    private func setupContainerView() {
        view.addSubview(signInView)
        
        signInView.snp.makeConstraints({(make) -> Void in
            make.edges.equalToSuperview()
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Sign the user out of any existing session that may exist. eg. When a new user registers an account and is redirected here
        
        registerKeyboardObservers()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //signOut()
    }

    override func viewWillDisappear(_ animated: Bool) {
        removeKeyboardObservers()
    }
    
    private func signOut(){
        do {
            try Auth.auth().signOut()
        } catch let signOutError as NSError {
            print("Error signing out %@", signOutError)
        }
    }
    
}



// This view controller exstension seperates the button click handlers from the main body of the controller
// for readability.
extension UserSignInController {
    // Force-end any editing / Resign on-screen keyboard and push the next view controller to the stack
    @objc func handleRegisterButtonTouch() {
        view.endEditing(true)
        signInView.emailTextField.text = nil
        signInView.passwordTextField.text = nil
        
        navigationController?.pushViewController(UserRegisterController(), animated: true)
    }
}



// The methods called when the textFields delegate is invoked
extension UserSignInController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Testing that it is always safe to force unwrap a textfields text
        //print("Text in TextField is:", textField.text!, "!")
        // Results show that it is perfectly fine, no crashes and saves alot of optional binding code..
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
}



extension UserSignInController: SignInViewDelegate {
    
    // Sign the user in with their email. Print error on fail.
    func signInButtonTapped() {
        
        Auth.auth().signIn(withEmail: self.signInView.emailTextField.text!, password: self.signInView.passwordTextField.text!, completion: { (user, error) in
            
            // Check if Firebase Auth set error, present alert if error, and return out
            if let error = error {
                let authErrorCode = AuthErrorCode(rawValue: error._code)!
                FirebaseError.Auth.present(authErrorCode, from: self)
                return
            }
            
            // Make sure the user object is not nil (authentication successful)
            if user != nil {
                let lastSignIn = FormatDate.formateDateToIntegerTimeInterval(date: Date())
                Database.database().reference().child("users/\(user!.uid)/lastSignIn").setValue(lastSignIn)
                
                // present Dashboard View Controller
                self.present(NavigationController(rootViewController: DashboardTabBarController()), animated: true, completion: nil)
            }
            
            // Remove text from within the textFields
            self.signInView.emailTextField.text = ""
            self.signInView.passwordTextField.text = ""
        })
    }
    
}



/// Class extension that handles the scrollView position when the on-screen keyboard is visible
extension UserSignInController {
    func registerKeyboardObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    func removeKeyboardObservers() {
        NotificationCenter.default.removeObserver(self, name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.removeObserver(self, name:NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    @objc func keyboardWillShow(notification:NSNotification){
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = signInView.scrollView.contentInset
        
        // Create an inset at the bottom of the scrollView which is equal to the height of the keyboard
        contentInset.bottom = keyboardFrame.size.height
        
        // Assign updated inset to the scrollView
        signInView.scrollView.contentInset = contentInset
    }
    
    @objc func keyboardWillHide(notification:NSNotification){
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        signInView.scrollView.contentInset = contentInset
    }
}


