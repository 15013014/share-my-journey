//
//  SignInView.swift
//  Share My Journey
//
//  Created by Robert John Britton on 14/04/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import UIKit

protocol SignInViewDelegate {
    func signInButtonTapped()
}

class SignInView: UIView {
    
    public var delegate: SignInViewDelegate?
    
    private let brandImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "SMJ_Logo_Wide")
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    
    private let textFieldContainerView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 10.0
        view.layer.borderWidth = 1.0
        view.layer.borderColor = UIColor(hex: Global.colour.lightGrey).cgColor
        view.clipsToBounds = true
        return view
    }()
    
    let emailTextField: BaseTextField = {
        let textField = BaseTextField()
        textField.validCharacters = Global.validCharSet.email
        //textField.text = "test@gmail.com"   /// remove after testing
        textField.placeholder = Global.placeholder.email
        textField.keyboardType = .emailAddress
        return textField
    }()
    
    private let emailtextFieldSeperator: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(hex: Global.colour.lightGrey)
        return view
    }()
    
    let passwordTextField: UITextField = {
        let textField = BaseTextField()
        textField.validCharacters = Global.validCharSet.password
        //textField.text = "password"         /// remove after testing
        textField.placeholder = Global.placeholder.password
        textField.isSecureTextEntry = true
        return textField
    }()
    
    private let signInButton: UIButton = {
        let button = UIButton() .defaultButton(title: Global.titleText.button.signin)
        button.addTarget(self, action: #selector(handleSignInButtonTouch), for: .touchUpInside)
        return button
    }()
    
    public let scrollView: UIScrollView = {
        let view = UIScrollView()
        view.isScrollEnabled = true
        return view
    }()
    
    private let contentView: UIView = {
        let view = UIView()
        return view
    }()
    
    override func didMoveToSuperview() {
        setupView()
    }
    
    private func setupView() {
        addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(brandImageView)
        contentView.addSubview(textFieldContainerView)
        textFieldContainerView.addSubview(emailTextField)
        textFieldContainerView.addSubview(emailtextFieldSeperator)
        textFieldContainerView.addSubview(passwordTextField)
        contentView.addSubview(signInButton)
        
        
        scrollView.snp.makeConstraints({(make) -> Void in
            make.top.left.right.bottom.equalTo(0)
        })
        
        contentView.snp.makeConstraints({(make) -> Void in
            make.top.left.right.bottom.equalTo(0)
            make.width.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(1).priority(250)
        })
        
        brandImageView.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(contentView.snp.top).offset(50)
            make.left.equalTo(contentView)
            make.right.equalTo(contentView)
            make.height.equalTo(150)
        })
        
        textFieldContainerView.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(brandImageView.snp.bottom).offset(50)
            make.left.equalTo(contentView.snp.left).offset(16)
            make.right.equalToSuperview().offset(-16)
            make.height.equalTo(101)
        })
        
        // Email
        emailTextField.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(textFieldContainerView)
            make.left.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalTo(50)
        })
        
        emailtextFieldSeperator.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(emailTextField.snp.bottom)
            make.left.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalTo(1)
        })
        
        // Password
        passwordTextField.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(emailtextFieldSeperator.snp.bottom)
            make.left.equalTo(textFieldContainerView)
            make.width.equalTo(textFieldContainerView)
            make.height.equalTo(50)
        })
        
        signInButton.snp.makeConstraints({(make) -> Void in
            make.top.equalTo(textFieldContainerView.snp.bottom).offset(16)
            make.left.equalTo(contentView.snp.left).offset(16)
            make.width.equalTo(contentView.snp.width).offset(-32)
            make.height.equalTo(50)
            make.bottom.equalTo(scrollView.snp.bottom)
        })
    }
    
    @objc private func handleSignInButtonTouch() {
        self.delegate?.signInButtonTapped()
    }
}
