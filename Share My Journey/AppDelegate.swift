//
//  AppDelegate.swift
//  Share My Journey
//
//  Created by Robert John Britton on 15/11/2017.
//  Copyright © 2017 Robert John Britton. All rights reserved.
//


import UIKit

// For the Authentication and Realtime Database
import Firebase

// For Auto Layout Constraints
import SnapKit

// For Specifying RGB Colour by Hex
import Hue



@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    // Override point for customization after application launch.
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // Initialise the main window that will provide containment for the UI Comonents for the application
        window = UIWindow(frame: UIScreen.main.bounds)
        
        // Make the window the key window and visible (becomes the active responder)
        window?.makeKeyAndVisible()
        
        // Create an instance of the UserSignInController
        let signInController = UserSignInController()
        
        // Set the signInController as the NavigationControllers' rootViewController
        let navigationController = NavigationControllerLight(rootViewController: signInController)
        
        // Set the navigationController as the windows' rootViewController
        window?.rootViewController = navigationController
        
        // Configures the firebase singleton class so that its availbale througout the entire application
        FirebaseApp.configure()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}


