//
//  Share_My_JourneyUITests.swift
//  Share My JourneyUITests
//
//  Created by Robert John Britton on 15/11/2017.
//  Copyright © 2017 Robert John Britton. All rights reserved.
//

import XCTest


class UserSignIn_UITests: XCTestCase {
    let validEmail: String = "smj-test@share-my-journey.com"
    let validPassword: String = "-0ij89uh66*-kkij1nnbv787gj(ik^p"
    
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    
    // Check that the user recieves an alert if attempting to sign in using an unregistered email.
    func testSignIn_WithUnregisteredEmail() {
        // Capture launched app instance
        let app = XCUIApplication()
        
        // Capture the user interactive view components
        let emailTextField = app.textFields["Email..."]
        let passwordTextField = app.secureTextFields["Password..."]
        let signInButton = app.buttons["Sign In"]
        
        // Capture the alert and  alert button
        let alert = app.alerts["Alert"]
        let alertButton = alert.buttons["Dismiss"]
        
        // Perform actions
        emailTextField.tap()
        emailTextField.typeText("iuhsdisdufhijsndfiuqwpoqj@dhdkjhdkjhdkucenenx.co.uk")
        passwordTextField.tap()
        passwordTextField.typeText("765&*G76Ujhj--kj(oiu*kkj@jkhhnBBm^_")
        signInButton.tap()
        
        // The error message we expect to see when the alert is presented.
        let alertMsg = "There is no account registered to the email provided"
        
        // Expected outcome
        // Will pass if the alert is presented, and the correct message within it is visible
        XCTAssertTrue(alert.staticTexts["\(alertMsg)"].exists)
        alertButton.tap()
    }
    
    
    // Check that the user recieves an alert if the correct email is provided, but with the wrong password.
    func testSignIn_WithInvalidPassword() {
        // Capture launched app instance
        let app = XCUIApplication()
        
        // Capture the user interactive view components
        let emailTextField = app.textFields["Email..."]
        let passwordTextField = app.secureTextFields["Password..."]
        let signInButton = app.buttons["Sign In"]
        
        // Capture the alert and  alert button
        let alert = app.alerts["Alert"]
        let alertButton = alert.buttons["Dismiss"]
        
        // Perform actions
        emailTextField.tap()
        emailTextField.typeText("\(validEmail)")
        passwordTextField.tap()
        passwordTextField.typeText("765&*G76Ujhj--kj(oiu*kkj@jkhhnBBm^_")
        signInButton.tap()
        
        // The error message we expect to see when the alert is presented.
        let alertMsg = "Unable to find an account matching the email/password provided"
        
        // Expected outcome
        // Will pass if the alert is presented, and the correct message within it is visible
        XCTAssertTrue(alert.staticTexts["\(alertMsg)"].exists)
        alertButton.tap()
    }
    
    // Check that an alert is presented when the user attempts to sign in without providing an email.
    func testSignIn_WithNoEmailProvided() {
        // Capture launched app instance
        let app = XCUIApplication()
        
        // Capture the user interactive view components
        let passwordTextField = app.secureTextFields["Password..."]
        let signInButton = app.buttons["Sign In"]
        
        // Capture the alert and  alert button
        let alert = app.alerts["Alert"]
        let alertButton = alert.buttons["Dismiss"]
        
        // Perform actions
        passwordTextField.tap()
        passwordTextField.typeText("765&*G76Ujhj--kj(oiu*kkj@jkhhnBBm^_")
        signInButton.tap()
        
        // The error message we expect to see when the alert is presented.
        let alertMsg = "You have entered an invalid email"
        
        // Expected outcome
        // Will pass if the alert is presented, and the correct message within it is visible
        XCTAssertTrue(alert.staticTexts["\(alertMsg)"].exists)
        alertButton.tap()
    }
    
    // Check that an alert is presented when the user enters a valid email with an empty password.
    func testSignIn_withNoPassword() {
        // Capture launched app instance
        let app = XCUIApplication()
        
        // Capture the user interactive view components
        let emailTextField = app.textFields["Email..."]
        let signInButton = app.buttons["Sign In"]
        
        // Capture the alert and  alert button
        let alert = app.alerts["Alert"]
        let alertButton = alert.buttons["Dismiss"]
        
        // The error message we expect to see when the alert is presented.
        let alertMsg_invalidPassword = "Unable to find an account matching the email/password provided"
        
        // Perform actions
        emailTextField.tap()
        emailTextField.typeText("\(validEmail)")
        signInButton.tap()
        
        // Expected outcome
        // Will pass if the alert is presented, and the correct message within it is visible
        XCTAssertTrue(alert.staticTexts["\(alertMsg_invalidPassword)"].exists)
        alertButton.tap()
    }
    
    // Check that no alert is presented when the user enters the correct login information.
    func testSignIn_WithValidSignInInformation() {
        // Capture launched app instance
        let app = XCUIApplication()
        
        // Capture the user interactive view components
        let emailTextField = app.textFields["Email..."]
        let passwordTextField = app.secureTextFields["Password..."]
        let signInButton = app.buttons["Sign In"]
        
        // Capture the alert and  alert button
        let alert = app.alerts["Alert"]
        
        // Perform actions
        emailTextField.tap()
        emailTextField.typeText("\(validEmail)")
        passwordTextField.tap()
        passwordTextField.typeText("\(validPassword)")
        signInButton.tap()
        
        // Expected outcome
        // Will pass if the no alert is presented
        XCTAssertFalse(alert.staticTexts[""].exists)
    }
}
