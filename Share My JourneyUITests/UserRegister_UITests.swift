//
//  UserRegister_UITests.swift
//  Share My JourneyUITests
//
//  Created by Robert John Britton on 03/03/2018.
//  Copyright © 2018 Robert John Britton. All rights reserved.
//

import XCTest

class UserRegister_UITests: XCTestCase {
    let existingEmail: String = "smj-test@share-my-journey.com"
    
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testUserRegister_WithPreExistingEmail() {
        // Capture launched app instance
        let app = XCUIApplication()
        app.navigationBars["Sign In"].buttons["Register"].tap()
        
        // Capture the user interactive view components
        let emailTextField = app.textFields["Email..."]
        let passwordTextField = app.secureTextFields["Password..."]
        let confirmPasswordTextField = app.secureTextFields["Confirm password..."]

        // Capture the alert and  alert button
        let alert = app.alerts["Alert"]
        let alertButton = alert.buttons["Dismiss"]
        
        // Perform actions
        emailTextField.tap()
        emailTextField.typeText("\(existingEmail)")
        passwordTextField.tap()
        passwordTextField.typeText("765&*G76Ujh")
        confirmPasswordTextField.tap()
        confirmPasswordTextField.typeText("765&*G76Ujh")
        
        app.navigationBars["Register"].buttons["Continue"].tap()
        
        // The error message we expect to see when the alert is presented.
        let alertMsg = "The email address provided is already registered"
        
        // Expected outcome:
        // Will pass if the alert is presented, and the correct message within it is visible
        XCTAssertTrue(alert.staticTexts["\(alertMsg)"].exists)
        alertButton.tap()
    }
    
    func testUserRegister_WithInvalidEmail() {
        // Capture launched app instance
        let app = XCUIApplication()
        app.navigationBars["Sign In"].buttons["Register"].tap()
        
        // Capture the user interactive view components
        let emailTextField = app.textFields["Email..."]
        let passwordTextField = app.secureTextFields["Password..."]
        let confirmPasswordTextField = app.secureTextFields["Confirm password..."]
        
        // Capture the alert and  alert button
        let alert = app.alerts["Alert"]
        let alertButton = alert.buttons["Dismiss"]
        
        // Perform actions
        emailTextField.tap()
        emailTextField.typeText("qqq")
        passwordTextField.tap()
        passwordTextField.typeText("765&*G76Ujh")
        confirmPasswordTextField.tap()
        confirmPasswordTextField.typeText("765&*G76Ujh")
        
        app.navigationBars["Register"].buttons["Continue"].tap()
        
        // The error message we expect to see when the alert is presented.
        let alertMsg = "You have entered an invalid email"
        
        // Expected outcome:
        // Will pass if the alert is presented, and the correct message within it is visible
        XCTAssertTrue(alert.staticTexts["\(alertMsg)"].exists)
        alertButton.tap()
    }
    
    func testUserRegister_WithMismatchPassword() {
        // Capture launched app instance
        let app = XCUIApplication()
        app.navigationBars["Sign In"].buttons["Register"].tap()
        
        // Capture the user interactive view components
        let emailTextField = app.textFields["Email..."]
        let passwordTextField = app.secureTextFields["Password..."]
        let confirmPasswordTextField = app.secureTextFields["Confirm password..."]
        
        // Capture the alert and  alert button
        let alert = app.alerts["Alert"]
        let alertButton = alert.buttons["Dismiss"]
        
        // Perform actions
        emailTextField.tap()
        emailTextField.typeText("testing123@gmail.com")
        passwordTextField.tap()
        passwordTextField.typeText("w3n_uj8^")
        confirmPasswordTextField.tap()
        confirmPasswordTextField.typeText("w3n_uj")
        
        app.navigationBars["Register"].buttons["Continue"].tap()
        
        // The error message we expect to see when the alert is presented.
        let alertMsg = "The passwords do not match"
        
        // Expected outcome:
        // Will pass if the alert is presented, and the correct message within it is visible
        XCTAssertTrue(alert.staticTexts["\(alertMsg)"].exists)
        alertButton.tap()
    }
    
    // Input validation prohibits the user from tapping the continue button when the email text field is empty
    // and the password text fields length is less than 6 characters (the minumum required by Firebase Auth).
    // So tests for these conditions are not required.
    
}
